<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appareil extends Model
{
    protected $table = 'appareil';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    protected $keyType = 'int';
    protected $attributes = [
        'nom' => null ,
        'prix' => null
    ];
}