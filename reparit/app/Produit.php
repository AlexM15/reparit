<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    protected $table = 'produit';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
    protected $keyType = 'int';
    protected $attributes = [
        'nom' => null,
        'budget' => 0,
        'prixDeVente' => 0,
        'description' => null,
        'datePublication' => null,
        'idUser' => 0,
        'aReparer' => 0,
        'aVendre' => 0 
    ];
     
    /**
    * Get the user associated with the product.
    */
    public function user(){
        return $this->hasOne('App\User', 'id');
    }

    public function problems()
    {
        return $this->belongsToMany('App\Probleme', 'problemeproduit', 'idProduit', 'idProbleme');
    }

    public function signalements()
    {
        return $this->hasMany('App\Signalements','idProduit');
    }
    public function images()
    {
        return $this->hasMany('App\Image', 'idProduit');
    }

    public function propositions(){
        return $this->hasMany('App\Proposition', 'idProduit');
    }

    public function getUser(){
        return User::find($this->idUser);
    }
}