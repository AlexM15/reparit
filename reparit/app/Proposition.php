<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposition extends Model
{
    protected $table = 'proposition';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
    protected $keyType = 'int';
    protected $attributes = [
        'idProduit' => 0,
        'idUserPropos' => 0,
        'prixBudget' => 0,
        'description' => '',
        'reparation' => 1,
        'statut' => 0
    ];

    public function produit()
    {
        return $this->hasOne('App\Produit', 'idProduit');
    }
    public function userPropos()
    {
        return $this->hasOne('App\User', 'idUserPropos');
    }

    public function getUser(){
        return User::find($this->idUserPropos);
    }

    public function getProduit(){
        return Produit::find($this->idProduit);
    }

}