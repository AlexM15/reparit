<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Produit;
use App\Image;
use App\Probleme;
use App\User;
use App\Avis;
use App\Motifs;
use App\Proposition;
use App\Signalements;
use App\Appareil;
use App\Estimation;
use App\Mail\ContactMessage;
use App\Mail\PropositionMail;
use \DateTime;
use DB;
use ReCaptcha\ReCaptcha;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $id = Auth::id();
        $auth = false;
        $fail = false;
        $userObj = null;
        $avisRecu = null;
        $avisEcrit = null;
        $addr = "";
        $city = "";
        $postal = "";

        if(! Auth::check()){
            return redirect('login');
        }
        else{
            $auth = true;
            $userObj = User::find($id);
            $avisRecu = Avis::where('idUserCommentaire',$id)->get();
            foreach ($avisRecu as $unAvis) {
                $date = $unAvis->dateAvis;
                $date = new DateTime($date);
                $unAvis->dateAvis = $date->format('d/m/Y');
            }
            $avisEcrit = Avis::where('idUserCommente',$id)->get();
            foreach ($avisEcrit as $unAvis) {
                $date = $unAvis->dateAvis;
                $date = new DateTime($date);
                $unAvis->dateAvis = $date->format('d/m/Y');
            }

            $fulladdr = $userObj->adresse;
            $addr = substr($fulladdr, 0, strrpos($fulladdr, ','));
            $postal = substr($fulladdr, strrpos($fulladdr, ',') + 2);
            $city = substr($postal, strpos($postal, ' ') + 1);
            $postal = substr($postal, 0, strrpos($postal, ' '));

            if($request->isMethod('post')){
                
                $prenom = $request->input('prenom');
                $nom = $request->input('nom');
                $email = $request->input('email');
                $adresse = $request->input('adresse') . ', ' . $request->input('postal') . ' ' . $request->input('ville');
                $newpass = $request->input('newpass');
                
                //vérification des champs
                if($prenom != "" && $nom != "" && $email != ""){
                    //mdp non changé, sauvegarde des infos
                    $modif = User::find($id);
                    $modif->nom = $nom;
                    $modif->prenom = $prenom;
                    $modif->adresse = $adresse;
                    $modif->email = $email;

                    if($newpass != ""){
                        //mdp changé, verif si correct
                        if($newpass === $request->input('newpasscon')){
                            //mdp correct
                            $modif->password = Hash::make($newpass);
        
                            try{
                                DB::beginTransaction();
            
                                $modif->save();
            
                                DB::commit();
            
                                $fail = "ok";

                                //actualisation des infos
                                $userObj = User::find($id);
                                $avisRecu = Avis::where('idUserCommentaire',$id)->get();
                                foreach ($avisRecu as $unAvis) {
                                    $date = $unAvis->dateAvis;
                                    $date = new DateTime($date);
                                    $unAvis->dateAvis = $date->format('d/m/Y');
                                }
                                $avisEcrit = Avis::where('idUserCommente',$id)->get();
                                foreach ( $avisEcrit as $unAvis) {
                                    $date = $unAvis->dateAvis;
                                    $date = new DateTime($date);
                                    $unAvis->dateAvis = $date->format('d/m/Y');
                                }

                                $fulladdr = $userObj->adresse;
                                $addr = substr($fulladdr, 0, strrpos($fulladdr, ','));
                                $postal = substr($fulladdr, strrpos($fulladdr, ',') + 2);
                                $city = substr($postal, strpos($postal, ' ') + 1);
                                $postal = substr($postal, 0, strrpos($postal, ' '));

                            }catch(Exception $e){
                                DB::rollback();
                                $fail = "db";
                            }
                        }
                        else{
                            //les mdp ne sont pas correct
                            $fail = "password";
                        }
                    }
                    else{
                        //mdp non changé, sauvegarde

                        try{
                            DB::beginTransaction();
        
                            $modif->save();
        
                            DB::commit();
        
                            $fail = "ok";

                            //actualisation des infos
                            $userObj = User::find($id);
                            $avisRecu = Avis::where('idUserCommentaire',$id)->get();
                            foreach ($avisRecu as $unAvis) {
                                $date = $unAvis->dateAvis;
                                $date = new DateTime($date);
                                $unAvis->dateAvis = $date->format('d/m/Y');
                            }
                            $avisEcrit = Avis::where('idUserCommente',$id)->get();
                            foreach ( $avisEcrit as $unAvis) {
                                $date = $unAvis->dateAvis;
                                $date = new DateTime($date);
                                $unAvis->dateAvis = $date->format('d/m/Y');
                            }

                            $fulladdr = $userObj->adresse;
                            $addr = substr($fulladdr, 0, strrpos($fulladdr, ','));
                            $postal = substr($fulladdr, strrpos($fulladdr, ',') + 2);
                            $city = substr($postal, strpos($postal, ' ') + 1);
                            $postal = substr($postal, 0, strrpos($postal, ' '));

                        }catch(Exception $e){
                            DB::rollback();
                            $fail = "db";
                        }
                    }
                }
                else{
                    //manque d'infos
                    $fail = "info";
                }
            }
        }

        return view('monProfil', [
            'auth' => $auth,
            'fail' => $fail,
            'userObj' => $userObj,
            'avisRecu' => $avisRecu,
            'avisEcrit' => $avisEcrit,
            'postal' => $postal,
            'city' => $city,
            'addr' => $addr
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function selectedUser(Request $request, $id)
    {
        $auth = false;
        $fail = false;
        $userObj = User::find($id);
        $avisRecu = Avis::where('idUserCommentaire',$id)->get();
        foreach ($avisRecu as $unAvis) {
            $date = $unAvis->dateAvis;
            $date = new DateTime($date);
            $unAvis->dateAvis = $date->format('d/m/Y');
        }
        $avisEcrit = Avis::where('idUserCommente',$id)->get();
        foreach ($avisEcrit as $unAvis) {
            $date = $unAvis->dateAvis;
            $date = new DateTime($date);
            $unAvis->dateAvis = $date->format('d/m/Y');
        }
        $addr = "";
        $city = "";
        $postal = "";
    
        if(Auth::check() && Auth::id() == $id){
            return redirect('user');
        }

        if($request->isMethod('post')){

            if(!Auth::check()){
                session(['redirection' => 'user/'.$id]);
                return redirect('login');
            }
            else{
                $dateAvis = new DateTime;
                

                $newAvis = new Avis;
                $newAvis->idUserCommente = $id;
                $newAvis->idUserCommentaire = Auth::id();
                $newAvis->dateAvis = $dateAvis->format('Y-m-d');
                $newAvis->note = $request->input('note');
                $newAvis->commentaire = $request->input('commentaire');

                if(Avis::where('idUserCommente',$id)->where('idUserCommentaire',Auth::id())->where('dateAvis',$dateAvis->format('Y-m-d'))->get() == []){
                    try{
                        DB::beginTransaction();
        
                        $newAvis->save();
        
                        DB::commit();
        
                        $fail = "ok";
        
                    }catch(Exception $e){
                        DB::rollback();
                        $fail = "db";
                    }
                }
                else{
                    $fail = "date";
                }                
            }
        }
    
        return view('monProfil', [
            'auth' => $auth,
            'fail' => $fail,
            'userObj' => $userObj,
            'avisRecu' => $avisRecu,
            'avisEcrit' => $avisEcrit,
            'postal' => $postal,
            'city' => $city,
            'addr' => $addr
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function deconnexion()
    {
        return redirect('login');
    }

    public function AuthRouteAPI(Request $request){
        return $request->user();
    }
}
