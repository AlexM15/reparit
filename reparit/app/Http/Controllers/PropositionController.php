<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Produit;
use App\Image;
use App\Probleme;
use App\User;
use App\Avis;
use App\Motifs;
use App\Proposition;
use App\Signalements;
use App\Appareil;
use App\Estimation;
use App\Mail\PropositionMail;
use Illuminate\Support\Facades\Mail;
use DB;
use ReCaptcha\ReCaptcha;


class PropositionController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function proposition(Request $request, string $idPro)
    {
        if(! Auth::check()){
            session(['redirection' => 'afficherProduit/' . $idPro]);
            return redirect('login');
        }else{
            $produit = Produit::where('id',$idPro)->get()[0];
            $id = Auth::id();
            if ($request->isMethod('post')){
    
                //Ajout d'une proposition
                $proposition = new Proposition();
    
                $proposition->idProduit = $produit->id;
                $proposition->idUserPropos = $id;
                $proposition->description = $request->input("description");
                $proposition->prixBudget = $request->input("nouveau-prix");
    
                if($produit->aVendre === 1){
                    $proposition->reparation = 0;
                }else{
                    $proposition->reparation = 1;
                }
    
                try{
                    DB::beginTransaction();
    
                    $proposition->save(); 
                    DB::commit();
                }catch(Exception $e){
                    DB::rollback();
                }

                //envoi mail
                $leUser = User::find($id);
                $leVendeur = User::find($produit->idUser);
                Mail::to($leVendeur->email)->send(new PropositionMail($proposition, $produit, $leUser));


                return redirect("afficherProduit/".$produit->id);
            }
            $budgetMin = Proposition::where('idProduit', $produit->id)->min('prixBudget');
            $prixVenteMax = Proposition::where('idProduit', $produit->id)->max('prixBudget');
        }
    
        return view('proposition', [
            "produit" => $produit,
            "id" => $id,
            'budgetMin' => $budgetMin,
            'prixVenteMax' => $prixVenteMax
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function consulterPropositions(Request $request, string $idPro)
    {
        if(! Auth::check()){
            session(['redirection' => 'consulterPropositions/' . $idPro]);
            return redirect('login');
        }else{
            if ($request->isMethod('post')){
                $proposition = Proposition::where('id',$request->input("id"))->first();
                $proposition->statut = true;
                try{
                    DB::beginTransaction();

                    $proposition->save(); 

                    DB::commit();
                }catch(Exception $e){
                    DB::rollback();
                }
            }

            $idValid = "";

            $propositions = Proposition::where('idProduit', $idPro)->orderBy("id", "desc")->get();
            foreach ($propositions as $proposition) {
                if($proposition->statut == 1){
                    $idValid = $proposition->id;
                }
            }
            return view('consulterPropositions',[
                "idValid" => $idValid,
                "propositions" => $propositions
            ]);
        }
    }

    public function mesPropositions(Request $request)
    {
        $id = Auth::id();
        if(! Auth::check()){

            session(['redirection' => 'mesPropositions']);
            return redirect('login');
        }else{
            if($request->isMethod('post')){
                $idProposition = $request->input('idProposition');

                $proposition = Proposition::find($idProposition);
                $produit = Produit::find($proposition->idProduit);

                if($id != $produit->idUser){
                    return redirect('default');
                }

                try{
                    DB::beginTransaction();               

                    $proposition->delete();                    

                    DB::commit();
                }catch(Exception $e){
            
                    DB::rollback();
                }   

                return redirect('mesPropositions');
            }else{
                $propositions = Proposition::where('idUserPropos',$id)->orderBy("id", "desc")->get();
                $propositionsList = [];
                foreach ($propositions as $proposition) {
                    if(count(Proposition::where('idProduit',$proposition->idProduit)->where('statut',1)->get()) == 0){
                        $propositionsList[] = [$proposition, false, Produit::where('id',$proposition->idProduit)->first()];
                    }
                    else{
                        $propositionsList[] = [$proposition, true, Produit::where('id',$proposition->idProduit)->first()];
                    }
                }
                return view('mesPropositions',[
                    "propositions" => $propositionsList
                ]);
            }  
        }
    }
}
