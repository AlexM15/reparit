<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Produit;
use App\Image;
use App\Probleme;
use App\User;
use App\Avis;
use App\Motifs;
use App\Proposition;
use App\Signalements;
use App\Appareil;
use App\Estimation;
use App\Mail\ContactMessage;
use App\Mail\PropositionMail;
use DB;
use ReCaptcha\ReCaptcha;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $produitsReparer = Produit::where('aReparer',true)->get();
        if(count($produitsReparer) > 5){
            $produitsReparer->slice(0, 5);
        }
        $produitsAcheter = Produit::where('aReparer',false)->get();
        if(count($produitsAcheter) > 5){
            $produitsAcheter->slice(0, 5);
        }
        return view('accueil',[
            "produitsReparer" => $produitsReparer,
            "produitsAcheter" => $produitsAcheter
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contacts(Request $request)
    {
        if($request->isMethod('post')){
            $recaptcha = new ReCaptcha('6LfpNtIUAAAAANaWzzuBdOlZBWl6W6XGH2MPYrjl');
            $resp = $recaptcha->verify($request->get('g-recaptcha-response'), $request->getClientIp());
            //var_dump($resp);

            if($resp->isSuccess()){
                Mail::to('contact@reparit.fr')->send(new ContactMessage($request->message, $request->sender, $request->nomProduit));
                return view('contacts', [
                    'sent' => true
                ]);
            }

            return view('contacts', [
                'sent' => 'captcha'
            ]);
        }
        else{
            return view('contacts', [
                'sent' => false
            ]);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function faireProposition()
    {
        return view('faireProposition');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function aPropos()
    {
        return view('aPropos', []);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function signaler()
    {
        return view('signaler',[
            "motifs" => json_decode(file_get_contents("./motifs.json")),
            'annonceSignalee' => "asus"
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function mentionsLegales()
    {
        return view('mentionsLegales', []);
    }
}
