<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Produit;
use App\Image;
use App\Probleme;
use App\User;
use App\Avis;
use App\Motifs;
use App\Proposition;
use App\Signalements;
use App\Appareil;
use App\Estimation;
use App\Mail\ContactMessage;
use App\Mail\PropositionMail;
use DB;
use ReCaptcha\ReCaptcha;
use Illuminate\Support\Facades\Mail;

class ProduitController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ajoutProduit(Request $request)
    {
        $id = Auth::id();
        if( ! Auth::check()){
            session(['redirection' => 'ajoutProduit']);
            return redirect('login');
        } else if ($request->isMethod('post')){

            $images = json_decode($request->input("images"));
            $problems = $request->get("problems");
            
            $produit = new Produit;
            $produit->nom = $request->input("nom");
            if($request->input("budget") != null && $request->input("prixDeVente") != null){
                $produit->prixDeVente = $request->input("prixDeVente");
                $produit->budget = $request->input("budget");
                $produit->aVendre = 1;
                $produit->aReparer = 1;
            }
            else{
                if($request->input("budget") == null){
                    $produit->prixDeVente = $request->input("prixDeVente");
                    $produit->aVendre = 1;
                }
                else if($request->input("prixDeVente") == null){
                    $produit->budget = $request->input("budget");
                    $produit->aReparer = 1;
                }
            }
            $produit->idUser = $id;//provisoire
            $produit->description = $request->input("description");
            $produit->datePublication = now()->format('Y-m-d');
            
            //var_dump($images);
            try{
                DB::beginTransaction();
                
                $produit->save();
                $produit->problems()->attach($problems);
                $i = 1;

                if(!file_exists("../public/thumb_produit")){
                    mkdir("../public/thumb_produit");
                }

                if (count($images) == 0) {
                    $imgBD = new Image;
                    $imgBD->chemin = 'images/placeholder.png';
                    $imgBD->idProduit= $produit->id;
                    $imgBD->save();
                } else {
                    foreach($images as $image){
                        $imgBD = new Image;
                        $chemin = 'thumb_produit/'.$produit->id.'_'.$i.'_'.$image->file;
                        $imgBD->chemin = $chemin;
                        $i++;
                        $imgBD->idProduit= $produit->id;
                        $imgBD->save();
                        file_put_contents("../public/$chemin", file_get_contents($image->src));
                    }
                }
                
                $produit->save(); 
                DB::commit();
            }catch(Exception $e){
                DB::rollback();

                //var_dump($e);
            }
            
        
            return redirect('mesProduits');
        } 
        else {
            return view('ajoutProduit', [
                "problems" => Probleme::all(),
                "estimations" => Estimation::all(),
                "appareils" => Appareil::all()
            ]);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reparer(Request $request)
    {
        if($request->isMethod('post')){
            $produitsReparer = Produit::where('aReparer',true)->where('nom', 'like', "%".$request->nomRecherche."%")->get();
        }
        else{
            $produitsReparer = Produit::where('aReparer',true)->get();
        }
        return view('reparer',[
            'recherche' => $request->nomRecherche,
            'listeProduitsReparer' => $produitsReparer
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function acheter(Request $request)
    {
        if($request->isMethod('post')){
            $produitsVendre = Produit::where('aReparer',false)->where('nom', 'like', "%".$request->nomRecherche."%")->get();
        }
        else{
            $produitsVendre = Produit::where('aReparer',false)->get();
        }
        return view('acheter',[
            'recherche' => $request->nomRecherche,
            'listeProduitsVendre' => $produitsVendre
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function afficherProduit(Request $request, $id)
    {
        $produit = Produit::where('id',$id)->get()[0];

        $user = User::where('id',$produit->idUser)->get()[0];
        $motifs = Motifs::all();
        $idConnected = Auth::id();

        $resteImages = $produit->images->all();

        $resteImages = array_slice($resteImages, 1, count($resteImages)-1);
        
        if(count($user->avis) != 0){
            $note = 0;
            foreach ($user->avis as $avis) {
                $note += $avis->note ;
            }
            
            $note = $note / count($user->avis);
        }
        else{
            $note = null;
        }

        if( ! Auth::check()){
            $connected = false;
            session(['redirection' => 'afficherProduit/' . $id]);
        }
        else{
            $connected = true;
        }
        
        if ($request->isMethod('post')){
            //Ajout d'un signalement
            $signalement = new Signalements();
            $motifs = $request->get("motifs");

            $signalement->idProduit = $produit->id;
            $signalement->description = $request->input("description");
            try{
                DB::beginTransaction();
                $signalement->save(); 

                $signalement->motifs()->attach($motifs);

                $signalement->save(); 
                DB::commit();
            }catch(Exception $e){
                DB::rollback();
            }
        }

        return view('afficherProduit', [
            "produit" => $produit,
            "resteImages" => $resteImages,
            "motifs" =>  Motifs::all(),
            "user" => $user,
            "note" => $note,
            "connected" => $connected,
            "idConnected" => $idConnected
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function mesProduits(Request $request)
    {
        $id = Auth::id();
        
        if(! Auth::check()){

            session(['redirection' => 'mesProduits']);
            return redirect('login');
        }else{
            if($request->isMethod('post')){
                $idProduit = $request->input('idProduit');

                $produit = Produit::find($idProduit);

                if($id != $produit->idUser){
                    return redirect('default');
                }

                try{
                    DB::beginTransaction();

                    //suppression des problèmes
                    $produit->problems()->detach();
                    $produit->problems()->delete();
                    
                    //suppression des images
                    $images = Image::where('idProduit',$idProduit)->get();
                    foreach($images as $image){
                        $image->produit()->dissociate();
                        $image->delete();

                        //suppression de l'image dans le dossier thumb_produit
                        if(file_exists("../public/$image->chemin")){
                            unlink("../public/$image->chemin");
                        }
                        else if(file_exists("../public/images/$image->chemin")){
                            unlink("../public/images/$image->chemin");
                        }
                    }
                    
                    //suppression des propositions
                    $props = Proposition::where('idProduit', $idProduit)->get();
                    foreach($props as $prop){
                        $prop->userPropos()->dissociate();
                        $prop->produit()->dissociate();
                        $prop->delete();
                    }
                    
                    //suppression des signalements
                    $signalements = Signalements::where('idProduit', $idProduit)->get();
                    
                    foreach($signalements as $signals){
                        $signals->produit()->dissociate();
                        $signals->motifs()->detach();
                        $signals->motifs()->delete();
                        
                        
                        $signals->delete(); 
                    }
                    $produit->delete();
                    

                    DB::commit();
                }catch(Exception $e){
            
                    DB::rollback();
                }   

                return redirect('mesProduits');
            }else{
                $produits = Produit::where('idUser',$id)->get();  
                return view('mesProduits',[
                    "produits" => $produits
                ]);
            }  
        }   
    }
}
