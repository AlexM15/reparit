<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motifs extends Model
{
    protected $table = 'motifs';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
    protected $keyType = 'int';
    protected $attributes = [
        'nom' => null  
    ];

    public function signalements()
    {
        return $this->belongsToMany('App\Signalements');
    }
}