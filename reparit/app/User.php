<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword;
use App\Notifications\ReinitialiserMotDePasseReparit;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom', 'prenom', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table = 'user';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
    protected $keyType = 'int';
    protected $attributes = [
        'password' => null,
        'nom' => "",
        'prenom' => "",
        'adresse' => "",
        'admin' => 0  
    ];

    /**
    * Get the user associated with the product.
    */


    public function produits()
    {
        return $this->belongsTo('App\Produit','idUser');
    }

    public function propositions()
    {
        return $this->hasMany('App\Proposition', 'idUserPropos');
    }

    public function avis()
    {
        return $this->hasMany('App\Avis','idUserCommente', 'id');
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ReinitialiserMotDePasseReparit($token));
    }
}
