<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estimation extends Model
{
    protected $table = 'estimation';
    protected $primaryKey = ['idAppareil', 'idProbleme'];
    public $timestamps = false;
    public $incrementing = false;
    protected $attributes = [
        'prixEstimation' => null  
    ];

    public function appareil(){
        return $this->hasOne('App\Appareil', 'idAppareil');
    }

    public function probleme(){
        return $this->hasOne('App\Probleme', 'idProbleme');
    }


}