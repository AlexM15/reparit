<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'image';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
    protected $keyType = 'int';
    protected $attributes = [
        'chemin' => null  
    ];

    public function produit()
    {
        return $this->belongsTo('App\Produit','id','local_key');
    }
}