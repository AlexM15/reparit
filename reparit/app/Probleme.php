<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Probleme extends Model
{
    protected $table = 'probleme';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    protected $keyType = 'int';
    protected $attributes = [
        'nom' => null  
    ];

    public function produits()
    {
        return $this->belongsToMany('App\Produit');
    }


}