<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signalements extends Model
{
    protected $table = 'signalement';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
    protected $keyType = 'int';
    protected $attributes = [
        'description' => null,
        'idProduit' => 0
    ];
     
    /**
    * Get the motifs associated with the signal.
    */
    public function motifs()
    {
        return $this->belongsToMany('App\Motifs', 'motifssignalement', 'idSignalement', 'idMotif');
    }

    public function produit()
    {
        return $this->belongsTo('App\Produit', 'idProduit');
    }
}
