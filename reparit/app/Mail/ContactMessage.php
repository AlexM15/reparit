<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMessage extends Mailable
{
    use Queueable, SerializesModels;

    protected $bodyMessage;
    protected $sender;
    protected $objet;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $bodyMessage, string $sender, string $objet)
    {
        $this->bodyMessage = $bodyMessage;
        $this->sender = $sender;
        $this->objet = $objet;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->sender)
                ->subject($this->objet)
                ->view('emails.contact')
                ->with([
                    'bodyMessage' => $this->bodyMessage
                ]);
    }
}
