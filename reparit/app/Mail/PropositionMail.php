<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Proposition;
use App\Produit;
use App\User;

class PropositionMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $proposition;
    protected $usermail;
    protected $produit;
    protected $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Proposition $proposition, Produit $produit, User $usermail)
    {
        $this->proposition = $proposition;
        $this->usermail = $usermail;
        $this->produit = $produit;

        if($produit->aReparer){
            $type = "réparation";
        }
        else{
            $type = "vente";
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contact@reparit.fr')
                ->subject("Proposition de " . $this->type . " pour votre appareil " . $this->produit->nom)
                ->view('emails.proposition')
                ->with([
                    'proposition' => $this->proposition,
                    'produit' => $this->produit,
                    'usermail' => $this->usermail,
                    'type' => $this->type
                ]);
    }
}
