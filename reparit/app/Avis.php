<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avis extends Model
{
    protected $table = 'avis';
    protected $primaryKey = ['idUserCommente', 'idUserCommentaire','dateAvis'];
    public $timestamps = false;
    public $incrementing = false;
    protected $attributes = [
        'idUserCommente' => 0,
        'idUserCommentaire' => 0,
        'dateAvis' => 2020-01-01,
        'note' => 0,
        'commentaire' => null
    ];

    public function userCommente()
    {
        return $this->belongsTo('App\User', 'idUserCommente');
    }

    public function userCommentaire()
    {
        return $this->belongsTo('App\User', 'idUserCommentaire');
    }

}