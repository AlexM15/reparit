<?php

use Illuminate\Http\Request;


Route::get('/', 'HomeController@index')->name("default");

Route::match(['get', 'post'], 'ajoutProduit', 'ProduitController@ajoutProduit')->name('ajoutProduit');

Route::match(['get', 'post'],'reparer', 'ProduitController@reparer')->name('reparer');

Route::match(['get', 'post'],'acheter', 'ProduitController@acheter')->name('acheter');

Route::match(['get', 'post'], 'contacts', 'HomeController@contacts')->name('contacts');

Route::match(['get', 'post'],'afficherProduit/{id}', 'ProduitController@afficherProduit')->name('afficherProduit');

Route::match(['get', 'post'],'proposition/{id}', 'PropositionController@proposition')->name('proposition');

Route::match(['get', 'post'],'consulterPropositions/{id}', 'PropositionController@consulterPropositions')->name('consulterPropositions');

Route::match(['get', 'post'],'mesPropositions', 'PropositionController@mesPropositions')->name('mesPropositions');

Route::match(['get', 'post'], 'mesProduits', 'ProduitController@mesProduits')->name('mesProduits');

Route::get('faireProposition', 'HomeController@faireProposition')->name('faireProposition');

Route::get('aPropos', 'HomeController@aPropos')->name('aPropos');

Route::fallback(function() {
    return view('404');
})->name('404');

Route::match(['get', 'post'], 'user', 'UserController@index')->name("monProfil");

Route::match(['get', 'post'], 'user/{id}', 'UserController@selectedUser')->name("userProfil");

Route::get('signaler', 'HomeController@signaler')->name('signaler');

Route::get('mentionsLegales', 'HomeController@mentionsLegales')->name('mentionsLegales');

Route::get('deconnexion', 'UserController@deconnexion')->name('deconnexion');

//AUTHENTIFICATION
Auth::routes();