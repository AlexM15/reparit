@extends('layout')

@section('title', 'Mes Produits')

@section('description', 'Liste des produits mis en ligne par un utilisateur.')

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/mesProduits.css') }}">
@endsection

@section('content')
<h1>Mes propositions</h1>
<!-- Liste d'annonces pour réparation produit -->
<div id="contenu">
    <!-- Pour chaque annonce faire -->
    @if( count($propositions) != 0)
        @foreach($propositions as $proposition)
            <hr>
            <div class="row rpt-block">
                <div id="item-right" class="d-flex flex-column justify-content-center">
                    <div id="nom-produit" class="d-flex flex-row">
                        <a href={{ route('afficherProduit', $proposition[2]->id) }}><h2 style="text-decoration: underline">{{$proposition[2]->nom}}</h2></a>
                    </div>
                    <div class="d-flex flex-row">
                        <p><strong>Prix : </strong>{{$proposition[0]->prixBudget}}</p>
                    </div>
                    <div class="d-flex flex-row">
                        <p>{{$proposition[0]->description}}</p>
                    </div>
                    @if ($proposition[1])
                        @if ($proposition[0]->statut)
                            <p style="color: green;">La proposition a été acceptée</p>
                        @else
                            <p style="color: red;" >La proposition a été refusée</p>
                        @endif
                    @else
                        <form method="post">
                            @csrf
                            <input type="hidden" name="idProposition" value="{{ $proposition[0]->id }}"/>
                            <input type="submit" id="supprannonce" class="rpt-button rpt-button-red" value="Supprimer la proposition"/>
                        </form>
                    @endif
                </div>
            </div>
        @endforeach
    @else
        <p class="align-text">Vous n'avez pas encore créé de proposition</p>
    @endif
</div>
@endsection