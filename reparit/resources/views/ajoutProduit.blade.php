@extends('layout')

@section('title', 'Ajouter un produit')

@section('description', 'Ici vous pouvez ajouter un produit à vendre ou à réparer.')

@section('headers')
<link rel="stylesheet" href="{{ asset("css/ajoutProduit.css") }}">
<script>
    const estimations = JSON.parse("{{ json_encode($estimations) }}".split("&quot;").join('"'));
    const appareils = JSON.parse("{{ json_encode($appareils) }}".split("&quot;").join('"'));
</script>
<script src="{{ asset("js/ajoutProduit.js") }}"></script>
@endsection

@section('content')
<h1>Ajouter un appareil</h1>
<form class="row justify-content-center"  method="post">
    @csrf
    <div id="block-image" class="col-md-5 rpt-block">
        <div id="image-list" class="row justify-content-center"></div>
        <hr>
        <p>Cliquez sur une image pour la supprimer.</p>
        <input id="add-image" type="file" accept="image/png, image/jpeg" multiple hidden>
        <input type="hidden" name="images" value="[]">
        <div>
            <label for="add-image" class="rpt-button">Ajouter une image</label>
            <label class="rpt-button rpt-button-red">Supprimer toutes les images</label>           
        </div>      
    </div>
    <div class="col-md-6 col-sm-11 rpt-block" id="block-desc">
        <div class="row justify-content-center">
            <label for="input-name" class="col-md-5 col-sm-10 justify-content-center"><b>Type d'appareil :</b></label>
            <select class="col-md-10 col-sm-11" name="appareils" required>
                @foreach($appareils as $appareil)
                    <option value="{{ $appareil->id }}">{{ $appareil->nom }}</option>
                @endforeach
            </select>

            <label for="input-name" class="col-md-5 col-sm-10 justify-content-center"><b>Modèle de l'appareil :</b></label>
            <input id="input-name" name="nom" class="col-md-10 col-sm-10 rpt-block" type="text" placeholder="Samsung Galaxy S9" required>
        </div>
        <div class="row justify-content-center invisible">espace</div>
        <div class="row justify-content-center">
            <select class="col-md-10 col-sm-11" name="problems[]" multiple="multiple" required>
                @foreach($problems as $problem)
                    <option value="{{ $problem->id }}">{{ $problem->nom }}</option>
                @endforeach
            </select>
        </div>
        <div class="row justify-content-center align-items-center">
            <label for="problem-desc" class="col-md-6 col-sm-10 col-12"><b>Description du problème :</b></label>
            <textarea id="problem-desc" name="description" class="col-md-10 col-sm-11 col-12 rpt-block" rows="4" required></textarea>
        </div>
    </div>
    <div class="col-md-12 col-sm-12" id="text-info"><p><b>*</b> <i>Vous pouvez à la fois réparer et vendre votre appareil.</i></p></div>
    <div class="col-md-6 col-sm-11 rpt-block" id="block-reparer">
        <h4>Je veux faire réparer mon appareil</h4>
        <div class="d-flex flex-row flex-wrap align-items-center justify-content-around">
            <label for="problem-desc" class="flex-column"><b>Prix de réparation estimé :</b></label>
            <label class="flex-column"><span id="prix-reparation-estime">70</span></label>
        </div>
        <div class="d-flex flex-row flex-wrap align-items-center justify-content-around">
            <label class="flex-column">Votre budget :</label>
            <input class="flex-column rpt-block" name="budget" type="number" id="budget" >
        </div>
    </div>
    <div class="col-md-5 col-sm-11 rpt-block" id="block-vendre">
        <h4>Je veux vendre mon appareil</h4>
        <div class="d-flex flex-row flex-wrap align-items-center justify-content-around">
            <label class="flex-column"><b>Prix de vente conseillé :</b></label>
            <label class="flex-column"><span id="prix-vente-estime">70</span></label>
        </div>
        <div class="d-flex flex-row flex-wrap align-items-center justify-content-around">
            <label class="flex-column">Votre prix de vente : </label>
            <input class="flex-column rpt-block" name="prixDeVente" type="number" id="prixVente">
        </div>
    </div>
    
    
    <div class="col-md-8 col-sm-11">
        <input class="rpt-button col-md-12" type="submit">
    </div>
</form>
@endsection
