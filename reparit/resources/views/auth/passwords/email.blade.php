@extends('layout')

@section('title', "Réinitialiser le mot de passe")

@section('description',"Réinitialiser votre mot de passe RéparIT")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')
<div id="login-page">
    <div id="login" class="rpt-block">
        <h1>Réinitialiser votre mot de passe</h1>
        <hr>
        <div class="login-content align-text">
            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <p>
                    <strong>Adresse mail :</strong>
                    <div class="col-5 align-item">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </p>

                <button type="submit" class="rpt-button">
                    Envoyer le lien de réinitialisation
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
