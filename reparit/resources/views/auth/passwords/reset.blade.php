@extends('layout')

@section('title', "Réinitialiser le mot de passe")

@section('description',"Réinitialiser votre mot de passe RéparIT")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')
<div id="login-page">
    <div id="login" class="rpt-block">
        <h1>Réinitialiser votre mot de passe</h1>
        <hr>
        <div class="login-content align-text">
            <form method="POST" action="{{ route('password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <p>
                    <strong>Adresse mail :</strong>
                    <div class="col-5 align-item">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </p>

                <p>
                    <strong>Mot de passe : (8 caractères minimum)</strong>
                    <div class="col-5 align-item">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </p>

                <p>
                    <strong>Confirmez le mot de passe :</strong>
                    <div class="col-5 align-item">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </p>


                <button type="submit" class="rpt-button">
                    Réinitialiser
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
