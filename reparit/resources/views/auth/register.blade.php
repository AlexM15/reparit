@extends('layout')

@section('title', "S'enregistrer")

@section('description',"S'inscrire à Répar'IT.")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
        function onSubmit(token) {
          document.getElementById("registerform").submit();
        }
    </script>
@endsection

@section('content')  
<div id="login-page">
    <div id="login" class="rpt-block">
        <h1>Créez votre compte</h1>
        <hr>
        <div class="login-content align-text">
            <form id="registerform" method="post" action="{{ route('register') }}">
                @csrf

                <p>
                    <strong>Prenom :</strong>
                </p>
                <br/>
                <p>
                    <input name="prenom" type="text" class="form-control align-text @error('prenom') is-invalid @enderror" placeholder="Miku" required autocomplete="prenom" autofocus>
                    @error('prenom')
                        <strong style="color: red;">Veuillez remplir ce champ.</strong>
                    @enderror
                </p>
                <br/>
                <p>
                    <strong>Nom :</strong>
                </p>
                <br/>
                <p>
                    <input name="nom" type="text" class="form-control align-text @error('nom') is-invalid @enderror" placeholder="Hatsune" required autocomplete="nom">
                    @error('nom')
                        <strong style="color: red;">Veuillez remplir ce champ.</strong>
                    @enderror
                </p>
                <br/>
                <p>
                    <strong>Adresse mail :</strong>
                </p>
                <br/>
                <p>
                    <input name="email" type="email" class="form-control align-text @error('email') is-invalid @enderror" placeholder="mail@exemple.com" required autocomplete="email">
                    @error('email')
                        <strong style="color: red;">Erreur sur l'adresse mail (déjà utilisée ?)</strong>
                    @enderror
                </p>
                <br/>
                <p>
                    <strong>Mot de passe :</strong>
                </p>
                <br/>
                <p>
                    <input name="password" type="password" class="form-control align-text @error('password') is-invalid @enderror" placeholder="************" required>
                    @error('password')
                        <strong style="color: red;">Erreur sur le mot de passe : celui-ci doit comporter 8 caractères minimum.</strong>
                    @enderror
                </p>
                <br/>
                <p>
                    <strong>Mot de passe (confirmation) :</strong>
                </p>
                <br/>
                <p>
                    <input name="password_confirmation" type="password" class="form-control align-text" placeholder="************" required>
                </p>
                <br/>
                <button type="submit" class="rpt-button g-recaptcha" data-sitekey="6LfpNtIUAAAAAC6vnxrIsa42aXYCGjMXET0Xhb-T" data-callback='onSubmit'>Valider</button>
            </form>
        </div>
    </div>
</div>
@endsection
