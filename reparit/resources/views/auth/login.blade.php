@extends('layout')

@section('title', 'Se connecter')

@section('description',"Ici vous pourrez vous connecter à notre plateforme Répar'IT.")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')  
<div id="login-page">
    <div id="login" class="rpt-block">
        <h1>Se connecter</h1>
        <hr>
        <div class="login-content align-text">
            <form method="post" action="{{ route('login') }}">
                @csrf

                @error('email')
                    <strong style="color: red;">Adresse mail ou mot de passe invalide !</strong>
                @enderror
                <p>
                    <strong>Adresse mail :</strong>
                </p>
                <p>
                    <input name="email" type="email" class="form-control align-text @error('email') is-invalid @enderror" placeholder="mail@exemple.com" required autocomplete="email" autofocus>
                </p>
                <br/>
                <p>
                    <strong>Mot de passe :</strong>
                </p>
                <input name="password" type="password" class="form-control align-text @error('password') is-invalid @enderror" placeholder="************">
                @error('password')
                    <strong style="color: red;">Mot de passe incorrect !</strong>
                @enderror

                <br/><br/>
                <input type="submit" class="rpt-button" value="Connexion">
            </form>
            <br/>
            Pas de compte ? <a href="{{ route('register') }}">S'enregistrer</a>
            <br/><br/>
            <a href="{{ route('password.request') }}">Mot de passe oublié ?</a>
        </div>
    </div>
</div>
@endsection
