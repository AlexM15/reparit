@extends('layout')

@section('title', "À propos")

@section('description',"Ici vous en saurez plus sur Répar'IT, ses concepts, et son équipe.")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/aPropos.css') }}">
@endsection

@section('content')
<h1>À propos de Répar'IT</h1>
<div class="rpt-block">
    <h2>C'est quoi Répar'IT ?</h2>
    <p>Répar'IT est une plateforme de réparation et de mise en vente d'appareils électroniques cassés.
        Le but est de pouvoir limiter des achats de nouveaux ordinateurs, téléphones, et tablettes par exemple, ce qui est plus léger pour l'environnement, et votre portefeuille.
        Ce concept est né en 2019 d'un sujet de projet pour de jeunes développeurs comme notre équipe de 5 personnes, dont Alexandre Mazeau en est le chef.
    </p>
    <br/>
    <h2>À quoi je sers moi ?</h2>
    <p>Sur Répar'IT il existe 3 rôles dont vous pouvez tous faire partie :
        <ul>
            <li><strong>Le réparateur :</strong> Vous aimez réparer des trucs, bidouiller des systèmes électroniques ? Si cette simple question vous a énervé en la lisant de manque de termes professsionnels, ce rôle est fait pour vous ! Faites des propositions de réparation sur des produits de clients, sous forme d'enchères <a href="{{ route('reparer') }}">ici</a>. Vous pouvez aussi acheter des appareils cassés pour en récupérer des composants sur <a href="{{ route('acheter') }}">cette page</a>.</li>
            <li><strong>Le vendeur :</strong> Vous avez un appareil qui traine dans un placard ? On vous épargne le ménage de printemps si vous le mettez à vendre chez nous ! Des petites mains en prendront soin pour le dépouiller de ses composants ! Pour vendre votre appareil c'est juste <a href="{{ route('ajoutProduit') }}">ici</a>.</li>
            <li><strong>Le client :</strong> Vous voulez arrêter le gaspillage électronique ? Ou peut-être que simplement le fait de réparer votre téléphone tombé dans vos toilettes vous convient (on ne dira rien). Inscrivez-vous et remplissez un petit formulaire pour reçevoir des offres de réparateurs <a href="{{ route('ajoutProduit') }}">ici</a>.</li>
        </ul>
    </p>
</div>
<div class="rpt-block">
    <h2>Parlons de Green IT.</h2>
    <p>
        Le Green IT est une pratique consistant à limiter notre consommation électrique, et électronique dans le monde entier pour que l'environnement puisse en profiter.
        Aujourd'hui n'importe qui peut penser à ne pas laisser son ordinateur tourner toute la journée si nous nous trouvons pas devant. 
    </p>
    <p>
        Mais d'autres pratiques peuvent aider à baisser notre consommation, et pas seulement électrique, et ça commence par limiter sa consommation de composants électroniques.
        Répar'IT permet aux réparateurs de pouvoir récupérer les composants de vos téléphones, ordinateurs et tablettes (mais pas seulement), et ça, c'est un beau move pour la planète.
    </p>
    <p>
        Vous participez donc à limiter la surconsommation de terres rares, comme le cuivre et l'or par exemple.
    </p>
</div>
<div class="rpt-block">
    <h2>Mais leur idée est incroyable ! Qui sont-ils qu'on les couvre d'or ?</h2>
    <p>Alors, merci mais l'idée est d'abord un sujet de notre licence professionnelle centrée sur développement web.
        Ensuite, pour l'or, vous pouvez vous référer à notre équipe que voici :</p>
    <div class="d-flex flex-row item-equipe align-items-center">
        <div class="">
            <img src="{{ asset('images/alex.png') }}" alt="Ruby c'est sa passion" class="rpt-block">
        </div>
        <div class="texte-equipe">
            <p>Alexandre Mazeau est notre chef de projet grâce à ses capacités de leader innées. Il a su se démarquer par sa capacité à foncer tête baissée dans le projet, et se faire respecter par le reste de son équipe.</p>
            😩 <q>Mazeau mais que de nom !</q>
        </div>
    </div>
    <div class="d-flex flex-row item-equipe align-items-center">
        <div>
            <img src="{{ asset('images/nath.jpg') }}" alt="Un miku weeb en puissance" class="rpt-block">
        </div>
        <div class="texte-equipe">
            <p>Nathanaël Piquet est notre développeur spé back-end +2. Son calme est presque flippant mais on arrive à s'y faire.</p>
            🥬 <q>METEOOOOOOR !!!</q>
        </div>
    </div>
    <div class="d-flex flex-row item-equipe align-items-center">
        <div>
            <img src="{{ asset('images/greg.jpg') }}" alt="Un gros fan de Symphogear" class="rpt-block">
        </div>
        <div class="texte-equipe">
            <p>Gregory Abraham est notre développeur pro front, et il gère en JavaScript. Il parle très fort mais les décibels de sa voix sont égaux à ses compétences de dev.</p>
            <img id="emote" src="{{ asset('images/hibiki.png') }}" alt="Emote d'Hibiki, personnage de Symphogear"> <q>Watch Symphogear.</q>
        </div>
    </div>
    <div class="d-flex flex-row item-equipe align-items-center">
        <div>
            <img src="{{ asset('images/lucas.jpg') }}" alt="Un supporter des Girondins de Bordeaux" class="rpt-block">
        </div>
        <div class="texte-equipe">
            <p>Lucas Guillotin est l'un de nos deux dev full-stack, il apprend super vite le bougre. C'est compliqué de l'entendre parler de football car personne d'autre comprend mais c'est pas grave.</p>
            ⚽ <q>GOOOOOAAALLL !!!</q>
        </div>
    </div>
    <div class="d-flex flex-row item-equipe align-items-center">
        <div>
            <img src="{{ asset('images/enzo.jpg') }}" alt="Hackerman, un meme ambulant" class="rpt-block">
        </div>
        <div class="texte-equipe">
            <p>Enzo Fonteneau est l'autre dev full-stack, et s'occupe de la com tout en écrivant ces lignes. Il vous parlera toujours d'anecdotes de jeux vidéo sans contexte parce qu'il peut pas s'en empêcher. Déso ! Il a aussi réussi à unifier l'équipe grâce à un simple jeu de 52 cartes.</p>
            🃏 <q>Le Mao est un jeu qui se joue à quatre cartes...</q>
        </div>
    </div>
</div>
<div class="rpt-block">
    <h2>Nous suivre sur les réseaux sociaux c'est sympa.</h2>
    <p>Nous avons une page Twitter que vous pouvez suivre, on est assez actifs dessus et on poste des informations sur le développement de notre site !</p>
    <p><a href="https://twitter.com/ReparIT" target="new">Cliquez ici pour voir la page !</a></p>
    <br/>
    <p>Nous disposons aussi d'une page Facebook qui poste pour l'instant seulement dès qu'on effectue une grande avancée sur notre projet.</p>
    <p><a href="https://www.facebook.com/reparitcontact" target="new">Cliquez ici pour voir la page !</a></p>
</div>
@endsection