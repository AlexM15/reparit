

<form action="{{ route('afficherProduit', $produit->id) }}" class="d-flex flex-column rpt-block" >
        <div class="d-flex flex-row justify-content-center">
            <label><b>{{$produit->nom}}</b></label>
        </div>
        <div class="d-flex flex-row justify-content-center rpt-background rpt-block">
            <label class="retirer"></label>
            <img src="{{ asset($produit->images[0]->chemin) }}" alt="image de test" class="thumbnail rpt-block" />
        </div>
        <div class="d-flex flex-row justify-content-center">
            <label><b>Publié par</b> : <a href="{{ route('userProfil', $produit->idUser) }}" >{{ $produit->getUser()->prenom}} {{strtoupper( substr($produit->getUser()->nom, 0, 1)) }}.</a></label>
        </div>
        @if( $produit->aReparer)
            <div class="d-flex flex-row justify-content-center">
                <img src="{{ asset('images/picto_reparer.png') }}" alt="Icone de réparation d'appareil" class="picto"/><label class="label"> Budget de réparation : {{$produit->budget }} €</label>
            </div>
        @else
            <div class="d-flex flex-row justify-content-center">
                <img src="{{ asset('images/picto_acheter.png') }}" alt="Icone d'achat d'appareil" class="picto"/><label class="label"> Prix d'achat : {{$produit->prixDeVente }} €</label>
            </div>
        @endif
        <div class="d-flex flex-row justify-content-center">
            <label>Date de publication : {{ strftime('%d-%m-%Y',strtotime($produit->datePublication)) }}</label>
        </div>
        <div class="d-flex flex-row justify-content-center">
            <input class="btn rpt-button col-md-6 col-sm-8 col-10" type="submit" value="Consulter"/>
        </div>
    </form>