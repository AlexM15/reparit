@extends('layout')

@section('title', 'Consulter propositions')

@section('description', 'Liste des propositions pour votre appareil.')

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/consulterPropositions.css') }}">
@endsection

@section('content')
<h1>Propositions pour votre annonce</h1>
<!-- Liste de propositions pour une annonce -->
<div id="contenu">
    <!-- Pour chaque proposition faire -->
    @if( count($propositions) != 0)
        @foreach($propositions as $proposition)
            <hr>
            <div class="row rpt-block">
                <div id="item-right" class="d-flex flex-column justify-content-center">
                    <div id="nom-produit" class="d-flex flex-row">
                        <h2><strong>Proposition de : </strong>{{$proposition->getUser()->prenom}} {{$proposition->getUser()->nom[0]}}.</h2>
                    </div>
                    <div class="d-flex flex-row">
                        <p><strong>Prix : </strong>{{$proposition->prixBudget}}</p>
                    </div>
                    <div class="d-flex flex-row">
                        <p>{{$proposition->description}}</p>
                    </div>
                    @if ($idValid != "")
                        @if ($idValid == $proposition->id)
                            <p style="color: green;">La proposition a été acceptée</p>
                        @else
                            <p style="color: red;" >La proposition a été refusée</p>
                        @endif
                    @else
                        <form action="" method="POST">
                            @csrf
                            <input type="text" hidden value={{$proposition->id}} name="id" />
                            <input type="submit" value="Accepter la proposition"class="rpt-button" />
                        </form>
                    @endif
                </div>
            </div>
        @endforeach
    @else
        <p class="align-text">Vous n'avez pas de proposition pour ce produit.</p>
    @endif
</div>
@endsection