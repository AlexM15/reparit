@extends("layout")

@section("title", 'Page 404')

@section('description', 'Vous avez atteint la mauvaise page !')

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/404.css') }}">
@endsection

@section("content")
  <div id="contenu-404" class="align-items-center justify-content-around">
    <h1 class="center">404, cette page n'existe pas !</h1>
    <p id="description" class="center">Oups, quelqu'un s'est trompé pour arriver sur cette page... Désolé ?</p>
  </div>
@endsection