@extends('layout')
@section("title", "Accueil")
@section('description', "La plateforme de réparation d'appareils électroniques qui vous facilite la vie.")

@section('headers')
<link rel="stylesheet" href="{{ asset('css/accueil.css') }}">
<link rel="stylesheet" href="{{ asset('css/reparer_acheter.css') }}">
<script rel="stylesheet" src="{{ asset("js/index.js") }}"></script>
<script rel="stylesheet" src="{{ asset("js/accueil.js") }}"></script>
@endsection

@section('content')  

<h1 class='col-12'>Bienvenue sur Répar'IT !</h1>
<div class="container justify-content-center">
    <div class="row justify-content-center">
        <a class="col-md-3 col-sm-8 col-10 text-center rpt-block info rpt-pulse" href="{{ route('reparer') }}">
            <img src="{{ asset('images/icone_reparer0.png') }}" alt="Icone réparation d'ordinateur" class="reduire"/>
            <div class="row"><p>Vous êtes passionné de réparation ou de bricolage d'appareils numériques ? <br/>Notre grand panel d'annonces saura vous satisfaire.</p></div>
        </a>
        <a class="col-md-3 col-sm-8 col-10 text-center rpt-block info rpt-pulse" href="{{ route('ajoutProduit') }}">
            <img src="{{ asset('images/user0.png') }}" alt="Icone réparation d'ordinateur" class="reduire"/>
            <div class="row"><p>Vous voulez faire réparer l'un de vos appareils cassés ? <br/>Rien de plus simple : inscrivez-vous sur Répar'IT et créez une annonce concernant votre appareil cassé en indiquant ses défauts.</p></div>
        </a>
        <a class="col-md-3 col-sm-8 col-10 text-center rpt-block info rpt-pulse" href="{{ route('acheter') }}">
            <img src="{{ asset('images/icone_acheter0.png') }}" alt="Icone achat d'ordinateur" class="reduire"/>
            <div class="row"><p>Vous souhaitez acheter un appareil d'occasion à réparer vous-même pour le revendre ou pour votre propre consommation ?<br/>Alors cette page est faite pour vous.</p></div>
        </a>
    </div>
</div>
<hr>
<h2>Dernières annonces mises en ligne</h2>
<div class="row justify-content-center">
    <div class="custom-control custom-checkbox custom-control-inline">
        <input type="checkbox" class="custom-control-input" id="reparer" checked>
        <label class="custom-control-label" for="reparer">Je répare</label>
    </div>
    <div class="custom-control custom-checkbox custom-control-inline">
        <input type="checkbox" class="custom-control-input" id="acheter" checked>
        <label class="custom-control-label" for="acheter">J'achète</label>
    </div>
</div>
<div class="row invisible">ceci est invisible</div> 

<div class="d-flex flex-row justify-content-around align-items-start flex-wrap">
    <div id="blocReparer">
        <h3>Appareils à réparer</h3>
        <div class="d-flex flex-column" >
        @foreach($produitsReparer as $produit)
            <div class="d-flex flex-row justify-content-around espaC" >
                @include("formulaireAccueil")
            </div>
        @endforeach
        </div>
    </div>
    <div id="blocAcheter">
        <h3>Appareils à acheter</h3>
        <div class="d-flex flex-column">
            @foreach($produitsAcheter as $produit)
                <div class="d-flex flex-row justify-content-around espaC" >
                    @include("formulaireAccueil")
                </div>
            @endforeach
        </div>
    </div>
</div>

@if(!isset($_COOKIE["disclaimer"]))
<div id="cookies">
    <div id="close-cookies">x</div>
    Répar'IT utilise des cookies. <a class="btn-cookies">J'accepte.</a>
</div>
@endif

@endsection
