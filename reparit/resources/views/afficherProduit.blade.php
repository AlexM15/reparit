@extends('layout')

@section('title', 'Afficher produit')

@section('description', 'Description complète du produit avec possibilité de faire une proposition ou un signalement sur le produit.')

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/afficherProduit.css') }}">
    <script type="text/javascript" src="{{ asset('js/afficherProduit.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/signaler.js')}}"></script>
@endsection


@section('content')
<div id="contenu" class="d-flex flex-wrap flex-row align-items-center justify-content-around">
    <div id="contenu-image" class="d-flex flex-column">
        <h1>{{ $produit->nom }}</h1>
        <div id="carousel" class="carousel slide rpt-block" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target=".carousel" data-slide-to="0" class="active"></li>
                @for($i=1; $i < count($produit->images); $i++)
                <li data-target=".carousel" data-slide-to="{{ $i }}"></li>
                @endfor
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 img-fluid fit" style="object-fit: cover" src="{{ asset($produit->images[0]->chemin)}}" alt="Un ordi cassé">
                </div>
                @foreach($resteImages as $image)
                    <div class="carousel-item">
                        <img class="d-block w-100 img-fluid fit" style="object-fit: cover" src="{{ asset($image->chemin)}}" alt="Un ordi cassé">
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#carousel" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a> 
        </div>
    </div>
    <div id="details" class="d-flex flex-column justify-content-center rpt-block">
        <div>
            @if($produit->aVendre)
                <p><b>Prix de vente :</b> {{ $produit->prixDeVente }} €</p>
            @else 
                <p><b>Budget :</b> {{ $produit->budget }} €</p>
            @endif

            <p><strong>État :</strong>
                @if($produit->aReparer)
                    A Réparer
                @else
                    A Vendre
                @endif
            </p>            

            <p><b>Problème(s) : </b>
                <ul>
                    @foreach($produit->problems as $problem)
                        <li class="problem">{{$problem->nom}}</li>
                    @endforeach
                </ul>
            </p>
            <p><b>Description du problème :</b> {{ $produit->description }}</p>
            <p><b>Date de publication :</b> {{ strftime('%d-%m-%Y',strtotime($produit->datePublication)) }} </p>
            <p><b>Demandeur : </b><a href="{{ route('userProfil', $user->id) }}" >{{ $user->prenom}} {{ $user->nom[0]}}.</a><br/>
            @if ($note != null)
                <p><b>Avis moyen utilisateur :</b> {{$note}}/5</p>
            @else
                <p><b>Avis moyen utilisateur :</b> L'utilisateur n'a pas encore reçu de note.</p> 
            @endif
            
                
        </div>
        <div>
            @if(!$connected)
                <a href="{{ route('login') }}"><button id="btn-proposition" class="rpt-button">Faire une proposition</button></a>
            @else
                @if($user->id == $idConnected)
                    <button id="btn-proposition" class="rpt-button" onclick="userPropositionIsSame()">Faire une proposition</button>
                    <button id="signaler-desktop" class="rpt-button-red rpt-button" onclick="userSignalementIsSame()">Signaler cette annonce</button>
                    <button id="signaler-mobile" class="rpt-button-red rpt-button" onclick="userSignalementIsSame()"></button>
                    <a href="{{ route('consulterPropositions', $produit->id) }}"><button id="consulter" class="rpt-button rpt-button-blue">Consulter les propositions</button></a>
                @else
                    <a href="{{ route('proposition', $produit->id) }}"><button id="btn-proposition" class="rpt-button">Faire une proposition</button></a>
                    <button id="signaler-desktop" class="rpt-button-red rpt-button" onclick="popupSignalement()">Signaler cette annonce</button>
                    <button id="signaler-mobile" class="rpt-button-red rpt-button" onclick="popupSignalement()"></button>
                @endif
            @endif
            
        </div>
    </div>
</div>
<div id="popup-s">
    <form method="post" id="popup-inner" class="container rpt-block">
        @csrf
        <img id="close" src="{{ asset('images/close.png') }}">
        <div class="row justify-content-center">
            <label>Vous voulez signaler l'annonce de {{$produit->nom}}.</label>
        </div>
        <div class="row justify-content-center">
            <label><b>Pour quel motif souhaitez-vous signaler cette annonce ?</b></label>
        </div>
        <div id="problemes" class="row justify-content-center">
            <select class="js-basic-multiple form-control col-md-8 col-sm-7 col-10" name="motifs[]" multiple="multiple">
                @foreach($motifs as $motif)
                    <option value="{{ $motif->id }}">{{ $motif->nom }}</option>
                @endforeach
            </select>
        </div>
        
        <div class="row justify-content-center">
            <label><u>Argumentation</u></label>
        </div>
        <div class="row justify-content-center">
            <textarea rows="5" name="description" class="col-md-8 col-sm-10 col-11 rpt-block"></textarea>
        </div>
        <div class="row justify-content-center">
            <input class="btn rpt-button" type="submit" value="Envoyer"/>
        </div>
    </form>        
</div>
@endsection