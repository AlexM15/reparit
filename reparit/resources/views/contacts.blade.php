@extends('layout')

@section('title', "Contacter Répar'IT")

@section('description','Ici vous pouvez nous envoyer une recommandation ou une question concernant notre plateforme, ou notre fonctionnement.')

@section('headers')
<link rel="stylesheet" href="{{ asset("css/ajoutProduit.css") }}">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script>
    function onSubmit(token) {
        document.getElementsByName("contacts")[0].submit();
    }
</script>
@endsection

@section('content')
<h1>Envoyer un message à la direction de Répar'IT</h1>
<div class="row justify-content-center ">
    @if($sent === true)
        <br/>
        <h2>Votre message à bien été envoyé !</h2>
    @else
        @if($sent == "captcha")
            <br/>
            <span>Erreur lors de la validation du captcha !</span>
            <br/>
        @endif
        <form method="post" action="#" name="contacts" class="col-md-6 col-sm-10 rpt-block">
            @csrf
            <div class="row"><p class="invisible col-md-3 col-sm-5"></p></div>
            <div class='row justify-content-center align-items-center'>
                <label class="col-md-5 col-sm-10 col-11" for="nomProduit">Votre email : </label>
                <input class="col-md-7 col-sm-10 col-11 rpt-block" type="text" name="sender" placeholder="exemple@email.fr" required />
            </div>
            <div class='row justify-content-center align-items-center'>
                <label class="col-md-5 col-sm-10 col-11" for="nomProduit">Objet du message : </label>
                <input class="col-md-7 col-sm-10 col-11 rpt-block" type="text" name="nomProduit" placeholder="Objet ..." required />
            </div>
            <div class="row justify-content-center">
                <label for="message" class="col-12" ><u>Votre message</u></label>
                <textarea rows="4" name="message" class="col-md-12 col-sm-12 col-12 rpt-block" required></textarea>
            </div>
            <div class="row justify-content-center">
                <button class="btn rpt-button col-md-6 col-sm-8 col-10 g-recaptcha" type="submit" data-sitekey="6LfpNtIUAAAAAC6vnxrIsa42aXYCGjMXET0Xhb-T" data-callback='onSubmit'>Envoyer le message</button>
            </div>
            <div class="row"><p class="invisible col-md-3 col-sm-5"></p></div>
        </form>
    @endif
</div>
@endsection