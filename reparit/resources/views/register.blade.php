@extends('layout')

@section('title', "S'enregistrer")

@section('description',"S'inscrire à Répar'IT.")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection

@section('content')  
<div id="login-page">
    <div id="login" class="rpt-block">
        <h1>Créez votre compte</h1>
        <hr>
        <div class="login-content align-text">
            <form method="post" action="{{ route('register') }}">
                @csrf

                <p>
                    <strong>Nom d'utilisateur :</strong>
                    <div class="col-5 align-item">
                        <input name="name" type="text" class="form-control align-text @error('name') is-invalid @enderror" placeholder="MikuDesu" required autocomplete="name" autofocus>
                        @error('name')
                            <strong style="color: red;">Veuillez remplir ce champ.</strong>
                        @enderror
                    </div>
                </p>
                <p>
                    <strong>Adresse mail :</strong>
                    <div class="col-5 align-item">
                        <input name="email" type="email" class="form-control align-text @error('email') is-invalid @enderror" placeholder="mail@exemple.com" required autocomplete="email">
                        @error('email')
                            <strong style="color: red;">Erreur sur l'adresse mail (déjà utilisée ?)</strong>
                        @enderror
                    </div>
                </p>      
                <p>
                    <strong>Mot de passe :</strong>
                    <div class="col-5 align-item">
                        <input name="password" type="password" class="form-control align-text @error('password') is-invalid @enderror" placeholder="************" required autocomplete="new-password">
                        @error('password')
                            <strong style="color: red;">Erreur sur le mot de passe : celui-ci doit comporter 8 caractères minimum.</strong>
                        @enderror
                    </div>
                </p>
                <p>
                    <strong>Mot de passe (confirmation) :</strong>
                    <div class="col-5 align-item">
                        <input name="password_confirmation" type="password" class="form-control align-text" placeholder="************" required autocomplete="new-password">
                    </div>
                </p>
                <input type="submit" class="rpt-button g-recaptcha" data-sitekey="6LfpNtIUAAAAAC6vnxrIsa42aXYCGjMXET0Xhb-T" value="Valider">
            </form>
        </div>
    </div>
</div>
@endsection
