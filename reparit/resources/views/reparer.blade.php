@extends('layout')

@section('title', 'Réparer')

@section('description', 'Liste des produits mis en ligne attendant un réparateur.')

@section('headers')
<link rel="stylesheet" href="{{ asset('css/reparer_acheter.css') }}">
@endsection

@section('content')
<h1>Réparer un appareil</h1>

<form method="post" class="formpost" action="{{ route('reparer') }}">
    @csrf
    <input class="form-control mr-sm-2" type="search" name="nomRecherche" value="{{$recherche}}" placeholder="Search" aria-label="Search">
    <br/>
    <input class="btn rpt-button col-md-3 col-sm-5 col-7" type="submit" value="Rechercher"/>
</form>
<br/>
<!-- Liste d'annonces pour réparation produit -->
<div class="d-flex flex-row justify-content-around align-items-center flex-wrap">
    <!-- Pour chaque annonce faire -->
    @if( count($listeProduitsReparer) != 0)
        @foreach($listeProduitsReparer as $produit)
        <form method="get" action="{{ route('afficherProduit', $produit->id) }}" class="d-flex flex-column rpt-block" >
            <div>
                <div class="d-flex flex-row justify-content-center">
                    <label><b>{{$produit->nom}}</b></label>
                </div>
                <div class="d-flex flex-row justify-content-center rpt-background rpt-block">
                    <label class="retirer"></label>
                    <img src="{{$produit->images[0]->chemin}}" alt="image de test" class="thumbnail" />
                </div>
                <div class="d-flex flex-row justify-content-center">
                    <label><b>Publié par</b> : <a href="{{ route('userProfil', $produit->getUser()) }}"> {{$produit->getUser()->prenom}} {{$produit->getUser()->nom[0]}}.</a></label>
                </div>
                <div class="d-flex flex-row justify-content-center">
                    <img src="{{ asset('images/picto_reparer.png') }}" alt="Icone de réparation d'appareil" class="picto"/><label class="label"> Budget de réparation : {{$produit->budget }} €</label>
                </div>
                <div class="d-flex flex-row justify-content-center">
                    <label>Date de publication : {{ strftime('%d-%m-%Y',strtotime($produit->datePublication)) }}</label>
                </div>
                <div class="d-flex flex-row justify-content-center">
                    <input class="btn rpt-button col-md-6 col-sm-8 col-10" type="submit" value="Consulter"/>
                </div>
            </div>
        </form>
        @endforeach
    @else
        <p class="align-text">Il n'y a pas de produit à réparer pour le moment.</p>
    @endif
</div>
<div class="row"><p class="invisible">ceci est invisible</div>

@endsection