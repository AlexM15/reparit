@extends('layout')

@section('title', 'Mentions Légales')

@section('description',"Ici vous pouvez consulter les mentions légales de Répar'IT")

@section('headers')
@endsection

@section('content')
    <h1>Mentions légales de Répar'IT</h1>
    <div class="rpt-block">
        <h2>INFORMATIONS GÉNÉRALES</h2>
        <p>Le site <a href="https://www.reparit.fr/">reparit.fr</a> est édité par Répar'IT, Société par Actions Simplifiée au modeste capital de 0 EUROS.
            Le siège social est situé au 15 rue de Naudet 33175 Gradignan.
            Le standard de Répar'IT est joignable  via notre formulaire de contact.</p>
        <p>Le représentant légal de la société Répar'IT est Alexandre Mazeau en sa qualité de CEO.
            Le directeur de la publication du site est Alexandre Mazeau.</p>
        <p>L’hébergement du nom de domaine est assuré par <a href="https://www.groupe-aquitem.fr/">Alienor</a>. La gestion et la sécurité du nom de domaine sont gérés par <a href="https://www.cloudflare.com/">Cloudflare</a>. Enfin, <a href="https://www.iut.u-bordeaux.fr/info/">L'IUT Informatique de Bordeaux</a> héberge notre site web.</p>
        <p>Le webdesign (création et développement) du site web est assuré par Répar'IT lui-même.</p>
    </div>
    <br/>
    <div class="rpt-block">
        <h2>DONNÉES PERSONNELLES</h2>
        <p>Vous disposez d’un droit d’accès, de modification, de rectification et de suppression des données personnelles que nous pourrions être amenés à recueillir (Art. 39 de la loi « Informatique et Libertés »). 
            Pour l’exercer, merci de nous contacter.</p>
    </div>
    <br/>
    <div class="rpt-block">
        <h2>DROITS D’AUTEUR</h2>
        <p>Le site web de <a href="https://www.reparit.fr/">reparit.fr</a> est une œuvre de l’esprit protégée par les lois de la Propriété Intellectuelle. 
            La totalité des images diffusées sont des éléments libres de droits d'auteur.</p>
    </div>
    <br/>
    <div class="rpt-block">
        <h2>LIMITATION DE RESPONSABILITÉ</h2>
        <p>Répar'IT déploie tous les moyens en sa possession afin d’assurer au mieux l’exactitude et la mise à jour des informations diffusées sur le site <a href="https://www.reparit.fr/">reparit.fr</a>. 
            Répar'IT décline toute responsabilité pour tous dommages résultant d’une intrusion frauduleuse d’un tiers ayant entraîné une modification des informations mises à la disposition sur le site et pour tous dommages, directs ou indirects, quelles qu’en soient les causes, origines, natures ou conséquences, provoqués aux dépends de la volonté et des moyens mis en œuvre par Répar'IT.</p>

        <p>Les photographies reproduisant les produits ne sont pas contractuelles. Le site est accessible 24 heures sur 24 et 7 jours sur 7, à l’exception des cas de force majeure. Pour des raisons de maintenance, Répar'IT pourra interrompre à tout moment le site.</p>
    </div>
@endsection