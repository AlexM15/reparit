@extends('layout')

@section('title', 'Mon profil')

@section('description', "Profil d'un utilisateur de Répar'IT")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/profil.css') }}">
@endsection

@section('content')
@if (! is_object($userObj))
    <!-- TO-DO: Page user non trouvée -->
    <div class="rpt-block align-text">
        <h1>Erreur !</h1>
        <h2>Cet utilisateur n'existe pas !</h2>
    </div>
@else
    @if ($auth === true)
        <div id="profil-page">
            <div id="profil" class="rpt-block">
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                
                <h1>Mon profil</h1>
                @if ($fail === "ok")
                    <p class="align-text">
                        <strong style="color: green;">Modifications enregistrés !</strong>
                    </p>
                @else
                    @if ($fail === "info")
                        <p class="align-text">
                            <strong style="color: red;">Veuillez renseigner la partie "Informations Utilisateur" !</strong>
                        </p>
                    @endif
                    @if ($fail === "password")
                        <p class="align-text">
                            <strong style="color: red;">Les champs 'mot de passe' ne correspondent pas !</strong>
                        </p>
                    @endif
                    @if ($fail === "db")
                        <p class="align-text">
                            <strong style="color: red;">Erreur interne, désolé du dérangement ! Nous travaillons dûr pour corriger le problème...</strong>
                        </p>
                    @endif
                @endif
                <form action="{{ route('logout') }}" method="get">
                    <button class="align-item-force rpt-button rpt-button-red" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Se déconnecter</button>
                </form>
                <hr>
                <div class="profil-content align-text">
                    <h2>Informations Utilisateur</h2>
                    <br/>
                    <form action="#" method="post">
                        @csrf
                        <p>
                            <strong>Prénom / Nom :</strong>
                        </p>
                        <br/>
                        <p>
                            <input name="prenom" type="text" class="form-control align-text" placeholder="Prénom" value="{{ $userObj->prenom }}" required>
                            <input name="nom" type="text" class="form-control align-text" placeholder="Nom" value="{{ $userObj->nom }}" required>
                            <br/>
                        </p>
                        <br/>
                        <p>
                            <strong>Adresse mail :</strong>
                        </p>
                        <br/>
                        <p>
                            <input name="email" type="email" class="form-control align-text" placeholder="mail@exemple.com" value="{{ $userObj->email }}" required>
                        </p>
                        <br/>
                        <p>
                            <strong>Adresse de livraison :</strong>
                        </p>
                        <br/>
                        <p>
                            <input name="adresse" type="text" class="form-control align-text" placeholder="Adresse" value="{{ $addr }}" required>
                            <input name="postal" type="text" class="form-control align-text" placeholder="Code postal" value="{{ $postal }}" required>
                            <input name="ville" type="text" class="form-control align-text" placeholder="Ville" value="{{ $city }}" required>
                        </p>
                        <br/>
                        <hr>
                        <h3>Modifier votre mot de passe</h3>
                        <br/>
                        <p>
                            <strong>Nouveau mot de passe :</strong>
                        </p>
                        <div class="col-5 align-item">
                            <input name="newpass" id="newpass" type="password" class="form-control align-text" placeholder="************">
                        </div>
                        <br/>
                        <p>
                            <strong>Ressaisir le mot de passe :</strong>
                        </p>
                        <div class="col-5 align-item">
                            <input name="newpasscon" id="newpass" type="password" class="form-control align-text" placeholder="************">
                        </div>
                        <br/>
                        <input type="submit" class="rpt-button" value="Sauvegarder les modifications"/>
                    </form>
                    <br/>
                    <a href="{{ route('mesProduits') }}"><button class="rpt-button rpt-button-blue" >Consulter mes produits</button></a>
                </div>
            </div>
            
            <div id="avis" class="rpt-block">
                <h1>Mes avis</h1>
                <hr>
                <div class="avis-content align-text">
                    <h2>Avis reçus</h2>
                    <br/>
                    @if( count($avisRecu) != 0)
                        @foreach($avisRecu as $avi)
                            <a href="{{ route('userProfil', $avi->idUserCommente) }}"><u>Avis de <strong>{{App\User::find($avi->idUserCommente)->prenom}} : </strong></u></a><br/>
                            <p>Le {{$avi->dateAvis}}</p>
                            {{ $avi->commentaire }}
                            <br/><br/>
                        @endforeach
                    @else
                        <p>Vous n'avez pas encore reçu d'avis</p>
                    @endif
                    <hr>
                    <h2>Avis rédigés</h2>
                    <br/>
                    @if( count($avisEcrit) != 0)
                        @foreach($avisEcrit as $avi)
                            <a href="{{ route('userProfil', $avi->idUserCommentaire) }}"><u>Avis rédigé pour <strong>{{App\User::find($avi->idUserCommentaire)->prenom}} : </strong></u></a><br/>
                            <p>Le {{$avi->dateAvis}}</p>
                            {{ $avi->commentaire }}
                            <br/><br/>
                        @endforeach
                    @else
                        <p>Vous n'avez pas encore rédigé d'avis</p>
                    @endif
                </div>
            </div>
        </div>
    @else
        <div id="profil-page">
            <div id="profil" class="rpt-block">
                <h1>Profil de {{ $userObj->prenom }}</h1>
                <hr>
                <div class="profil-content align-text">
                    <h2>Informations Utilisateur</h2>
                    <br/>
                    <p>
                        <strong>Utilisateur :</strong><br/>
                        {{ $userObj->prenom }} {{ $userObj->nom }}
                    </p>
                </div>
                <hr>
                <div class="align-text">
                    @if ($fail === "ok")
                        <p class="align-text">
                            <strong style="color: green;">Avis enregistrés !</strong>
                        </p>
                    @else
                        @if ($fail === "date")
                            <p class="align-text">
                                <strong style="color: red;">Veuillez attendre demain avant de pouvoir faire un nouveau commentaire pour cet utilisateur.</strong>
                            </p>
                        @else
                            @if ($fail === "db")
                                <p class="align-text">
                                    <strong style="color: red;">Erreur interne, désolé du dérangement ! Nous travaillons dûr pour corriger le problème...</strong>
                                </p>
                            @endif
                        @endif
                    @endif
                    <h3>Donnez votre avis sur cet utilisateur</h3>
                    <form action="" method="post">
                        @csrf
                        <div class="col-5 align-item">
                            <p><strong>Notez l'utilisateur :</strong></p>
                            <input name="note" id="reviewNote" type="number" min="0" max="5" required class="form-control align-text" placeholder="0/5">
                        </div>
                        
                        <div class="col-5 align-item">
                            <p><strong>Laissez un commentaire :</strong></p>
                            <textarea name="commentaire" id="reviewComment" class="form-control align-text" placeholder="Tapez un commentaire..."></textarea>
                        </div>                        
                        <br/>
                        <input type="submit" class="rpt-button" value="Sauvegarder les modifications"/>
                    </form>
                </div>
            </div>
            <div id="avis" class="rpt-block">
                <h1>Avis de l'utilisateur</h1>
                <hr>
                <div class="avis-content align-text">
                    <h2>Avis reçus</h2>
                    <br/>
                    @if( count($avisRecu) != 0)
                        @foreach($avisRecu as $avi)
                            <a href="{{ route('userProfil', $avi->idUserCommente) }}"><u>Avis de <strong>{{App\User::find($avi->idUserCommente)->prenom}} : </strong></u></a><br/>
                            <p>Le {{$avi->dateAvis}}</p>
                            {{ $avi->commentaire }}
                            <br/><br/>
                        @endforeach
                    @else
                        <p>Vous n'avez pas encore reçu d'avis</p>
                    @endif
                    <hr>
                    <h2>Avis rédigés</h2>
                    <br/>
                    @if( count($avisEcrit) != 0)
                        @foreach($avisEcrit as $avi)
                            <a href="{{ route('userProfil', $avi->idUserCommentaire) }}"><u>Avis rédigé pour <strong>{{App\User::find($avi->idUserCommentaire)->prenom}} : </strong></u></a><br/>
                            <p>Le {{$avi->dateAvis}}</p>
                            {{ $avi->commentaire }}
                            <br/><br/>
                        @endforeach
                    @else
                        <p>Vous n'avez pas encore rédigé d'avis</p>
                    @endif
                </div>
            </div>
        </div>
    @endif
@endif
@endsection
