<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>@yield("title") | Répar'IT</title>
        <link rel="icon" href="{{ asset("images/favicon.ico") }}">
        <meta name="keywords" content="ReparIT, IT, Mobile, Ordinateur, Tablette, Montre, connectée, Enceinte, Iphone, Samsung, Huawei, Acer, HP, Android, Apple, Dell, Cassé, Occasion, Réparateur">
        <meta name="description" content="@yield("description")">
        <meta name="viewport" content="width=device-width, initial-scale=0.7">
        <meta charset="utf-8">
        <!-- JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <script rel="stylesheet" src="{{ asset("js/index.js") }}"></script>
        <!-- CSS -->       
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" href="{{ asset("css/style.css") }}">
        <link rel="stylesheet" href="{{ asset("css/layout.css") }}">
        <!-- Select2 -->
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
        <!-- fonts -->
        <link href="https://fonts.googleapis.com/css?family=Orbitron&display=swap" rel="stylesheet">
        @yield("headers")
    </head>
    <body>
        <header class="sticky-top rpt-shadow">
            <nav class="container-fluid navbar navbar-expand-md navbar-dark">
                <a class="navbar-brand" href="/"><img class="rpt-pulse" src="{{ asset("images/logoSmol.png") }}" width="40" height="40" alt="Logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('default') }}">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('ajoutProduit') }}">Ajouter un appareil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('reparer') }}">Réparer</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('acheter') }}">Acheter</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('mesProduits') }}">Mes produits</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('mesPropositions') }}">Mes propositions</a>
                        </li>
                    </ul>
                    <a id="popup" href="{{ route('login') }}"><img id="login-img" class="img-responsive" src="{{ asset('images/account.png') }}" alt="S'identifier / S'inscrire" width="40" height="40"/></a>
                </div>
            </nav>
        </header>
        <div id="content">
            @yield("content")
        </div>
        <footer class="container-fluid">
            <section class="row">
                <div class="col-md-2 col-sm-6 centrer"><a href="{{ route('default') }}">Accueil</a></div>
                <div class="col-md-2 col-sm-6 centrer_long"><a href="{{ route('ajoutProduit') }}">Ajouter un appareil</a></div>
                <div class="col-md-2 col-sm-6 centrer"><a href="{{ route('reparer') }}">Réparer</a></div>
                <div class="col-md-2 col-sm-6 centrer"><a href="{{ route('acheter') }}">Acheter</a></div>
                <div class="col-md-2 col-sm-6 icone"><a href="https://twitter.com/reparit" target="new"><img class="rpt-pulse" src="{{ asset('images/Twitter.png') }}" alt="notre twitter" /></a></div>
                <div class="col-md-2 col-sm-6 icone"><a href="https://www.facebook.com/reparitcontact/" target="new"><img class="rpt-pulse" src="{{ asset('images/Facebook.png') }}" alt="notre facebook"/></a></div>
            </section>
            <section class="row">
                <div class="col-md-4"><a href="{{ route('contacts') }}">Contactez-nous</a></div>
                <div class="col-md-4"><a href="{{ route('aPropos') }}">À propos</a></div>
                <div class="col-md-4"><a href="{{ route('mentionsLegales') }}">Mentions légales</a></div>     
                <div class="col-md-12"><p>© Copyright 2019-2020 Répar'IT, Tous droits réservés</p></div>
            </section>
        </footer>
        <!-- JS (Bootstrap) -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>