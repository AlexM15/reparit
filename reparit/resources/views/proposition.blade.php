@extends('layout')

@section('title', 'Proposition')

@section('description', 'Faites une proposition pour ce produit.')

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/proposition.css') }}">
@endsection


@section('content')
@if ($produit->aReparer)
    <h1>Votre offre de réparation pour {{$produit->nom}}</h1>
@else
    <h1>Votre offre d'achat pour {{$produit->nom}}</h1>
@endif

<div id="proposition" class="justify-content-center">
    <form class="rpt-block" method="post">
        @csrf
        <label for="prix-min">Meilleure offre pour ce produit :</label>
        <input id="prix-min" type="text" class="form-control align-text" value="@if ($produit->aReparer) {{$budgetMin}} @else {{$prixVenteMax}} @endif" disabled>
        <br/>
        <label for="nouveau-prix">Montant de votre offre :</label>
        <input id="nouveau-prix" name="nouveau-prix" type="number" class="form-control align-text" placeholder="Votre prix" required>
        <br/>
        <label for="description">Description de votre offre :</label>
        <br/>
        <textarea id="description" rows="4" name="description" class="rpt-block" required></textarea>
        <br/>
        <input type="submit" id="valider" class="rpt-button col-12" name="valider-proposition"></button>
    </form>
</div>
@endsection