@extends('layout')

@section('content')
    <ul>
    @foreach($produits as $produit)
        <li>{{$produit->nom}}</li>
        <ol>
        @foreach($produit->problems as $problem)
            <li>{{$problem->nom}}</li>
        @endforeach
        </ol>
        <p>User : {{$produit->idUser}}</p>
        @foreach($produit->images() as $image) 
        <img src="{{ asset($image->chemin) }}" alt="image du produit" />

        @endforeach
    @endforeach
    </ul>
@endsection