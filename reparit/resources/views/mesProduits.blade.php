@extends('layout')

@section('title', 'Mes Produits')

@section('description', 'Liste des produits mis en ligne par un utilisateur.')

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/mesProduits.css') }}">
@endsection

@section('content')
<h1>Mes produits</h1>
<!-- Liste d'annonces pour réparation produit -->
<div id="contenu">
    <!-- Pour chaque annonce faire -->
    @if( count($produits) != 0)
        @foreach($produits as $produit)
            <hr>
            <div class="row rpt-block">
                <div id="item-left" class="d-flex flex-column">
                    <div id="image-produit" class="d-flex flex-row">
                        <a href={{ route('afficherProduit',$produit->id) }}><img src=" {{ asset($produit->images[0]->chemin)}}" alt="{{ $produit->images[0]->chemin }}" class="thumbnail rpt-block" /></a>
                    </div>
                </div>
                <div id="item-right" class="d-flex flex-column justify-content-center">
                    <div class="d-flex flex-row">
                    <span class="nom-produit"><a href={{ route('afficherProduit',$produit->id) }}><strong> {{ $produit->nom }}</strong></a></span>
                    </div>
                    <div class="d-flex flex-row">
                        @if($produit->aVendre)
                            <p><strong>Prix de vente :</strong> {{ $produit->prixDeVente }} €</p>
                        @else
                            <p><strong>Budget :</strong> {{ $produit->budget }} €</p>
                        @endif 
                    </div>
                    <div class="d-flex flex-row">
                        <p><strong>Date de publication :</strong> {{ $produit->datePublication}} </p>
                    </div>
                    <div class="d-flex flex-row">
                        <p><strong>État :</strong>
                            @if($produit->aVendre)
                                A Vendre
                            @else
                                A Réparer
                            @endif
                        </p>
                    </div>
                    <a href={{ route('afficherProduit', $produit->id) }}><button class="rpt-button rpt-button-blue" >Consulter mon annonce</button></a>
                    <div class="row invisible">a</div>
                    <form method="post">
                        @csrf
                        <input type="hidden" name="idProduit" value="{{ $produit->id }}"/>
                        <input type="submit" id="supprannonce" class="rpt-button rpt-button-red" value="Supprimer l'annonce"/>
                    </form>
                </div>
            </div>
        @endforeach
    @else
        <p class="align-text">Vous n'avez pas encore ajouté de produit</p>
    @endif
</div>
@endsection