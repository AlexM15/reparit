$(document).ready(function(){
    $('#reparer').change(function(){
        if ( $(this).is(':checked')) {
            $('#blocReparer').show();
            $('#blocAcheter').children("div").addClass("flex-column").removeClass("flex-row justify-content-around flex-wrap");
        }
        else {
            if($('#acheter').is(':checked')){
                $('#blocReparer').hide();
                $('#blocAcheter').children("div").removeClass("flex-column").addClass("flex-row justify-content-around flex-wrap");
            }
            else{
                $('#reparer').prop( "checked", true );
            }
        }               
    });

    $('#acheter').change(function(){
        if ($(this).is(':checked')) {
            $('#blocAcheter').show();
            $('#blocReparer').children("div").addClass("flex-column").removeClass("flex-row justify-content-around flex-wrap");
        }
        else {
            if($('#reparer').is(':checked')){
                $('#blocAcheter').hide();
                $('#blocReparer').children("div").removeClass("flex-column").addClass("flex-row justify-content-around flex-wrap");
            }
            else{
                $('#acheter').prop( "checked", true );
            }
        }                   
    });
    $('#blocReparer #labelAcheter').hide();
    $('#blocAcheter #labelReparer').hide();

    setTimeout(function() {
        $("#cookies").fadeIn(200);
    }, 1000);

    $("#close-cookies, .btn-cookies").click(function() {
        let nDays = 999;
        let cookieName = "disclaimer";
        let cookieValue = "true";

        let today = new Date();
        let expire = new Date();
        expire.setTime(today.getTime() + 3600000*24*nDays);
        document.cookie = cookieName+"="+escape(cookieValue)+";expires="+expire.toGMTString()+";path=/";
        $("#cookies").fadeOut(200);
    }); 
});