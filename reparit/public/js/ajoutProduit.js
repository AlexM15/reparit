const Images = {
    _list: [],
    add(file) {
        return new Promise(resolve => {
            if (this._list.length == 4) return null;
            const reader = new FileReader();
            reader.onload = e => {
                const img = {file: file.name, src: e.target.result};
                this._list.push(img);
                this.update();
                resolve(img);
            };
            reader.readAsDataURL(file);
        }); 
    },
    remove(i) {
        this._list.splice(i, 1)[0];
        this.update();
    },
    clear() {
        this._list = [];
        this.update();
    },
    update() {
        $("input[name=images]").val(JSON.stringify(this._list));
        $("#image-list").children().remove();
        let i = 0;
        for (let {src} of this._list) {
            let id = i++;
            $("#image-list").append('<div class="col-5 rpt-block"></div>');       
            $("#image-list div:last-child").rptBackground(src);
            $("#image-list div:last-child").click(() => this.remove(id));
        }
        $("#image-list div").css("height", this._list.length > 2 ? "12vh" :"24vh");  
    }
}

const Estimations = {
    update() {
        if(this.appareil.nom == "Autre"){
            $prixVente = "Pas d'estimation possible";
            $prixReparation = "Pas d'estimation possible";
        }
        else if(this.estimations.length != 0 && (this.estimations[0].idProbleme == "9" || this.estimations[0].idProbleme == "5")){
            $prixVente = "Pas d'estimation possible";
            $prixReparation = "Pas d'estimaton possible";
        }
        else{
            $prixVente = this.prixVente + " €";
            $prixReparation = this.prixReparation + " €";
        }

        $("#prix-vente-estime").text($prixVente);
        $("#prix-reparation-estime").text($prixReparation);
    },
    get appareil() {
        return appareils.filter(appareil => appareil.id == $("#content select[name=appareils]").val()).shift();
    },
    get problemes() {
        let problems = $('#content select[name="problems[]"]').val();
        return estimations.filter(estimation => problems.includes(""+estimation.idProbleme));
    },
    get estimations() {
        let appareil = this.appareil;
        return this.problemes.filter(problem => problem.idAppareil == appareil.id);
    },
    get prixVente() {
        return this.appareil.prix - this.prixReparation;
    },
    get prixReparation() {
        return this.estimations.reduce((acc, val) => acc + val.prixEstimation, 0);
    }
}

$(document).ready(function(){
    // select 2
    $("#content select").select2({
        placeholder: 'Choisir un problème'
    });

    // images
    $("#block-image label:last-child").click(() => Images.clear());
    $("input[type=file]").change(function() {
        const file = this.files[0];
        if (file) Images.add(file);
    });

    //estimations problèmes
    Estimations.update();
    $("#content select").change(() => Estimations.update());

    // checkboxes
    $("#checkbox-reparer").change(function() {
        $("#budget").attr("disabled", !$(this).is(":checked"));
    });
    $("#checkbox-vendre").change(function() {
        $("#prixVente").attr("disabled", !$(this).is(":checked"));
    });
});