function popupSignalement(){
    $('#popup-s').show();
    $('#popup-s').addClass("animated fadeIn");
    $(".js-basic-multiple").select2({
        placeholder: 'Choisir un motif...'
    });
    $(window).scrollTop(0);
}

function userPropositionIsSame(){
    alert("Vous ne pouvez pas faire une proposition à votre propre annonce !")
}

function userSignalementIsSame(){
    alert("Vous ne pouvez pas signaler votre propre annonce !")
}

$(document).ready(function(){
    $('#close').click(function(){
        $('#popup-s').hide();
    });
});