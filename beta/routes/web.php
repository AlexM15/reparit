<?php

use Illuminate\Http\Request;

session_start();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $produits = json_decode(file_get_contents("./produits.json"));
    $users = json_decode(file_get_contents("./users.json"));
    $listeProduitUserVendre = [];
    $listeProduitUserReparer = [];
    foreach ($produits as $value) {
        if($value->aReparer == false){
            foreach ($users as $value2) {
                if($value->idUser == $value2->id){
                    $listeProduitUserVendre[] = [$value,$value2];
                }
            }
        }
    }
    foreach ($produits as $value) {
        if($value->aReparer == true){
            foreach ($users as $value2) {
                if($value->idUser == $value2->id){
                    $listeProduitUserReparer[] = [$value,$value2];
                }
            }
        }
    }
    return view('accueil',[
        "listeProduitUserReparer" => $listeProduitUserReparer,
        "listeProduitUserVendre" => $listeProduitUserVendre
    ]);
})->name("default");

Route::match(['get', 'post'], 'ajoutProduit', function(Request $request) {
    if( !isset($_SESSION['user']) || ( $_SESSION['user'] === "-1" || $_SESSION['user'] === false )){
        $_SESSION['redirection'] = "ajoutProduit";
        return redirect('login');
    } else if ($request->isMethod('post'))
        return redirect('mesProduits');
    else {
        return view('ajoutProduit', [
            "problems" => json_decode(file_get_contents("./problems.json"))
        ]);
    }
})->name('ajoutProduit');

Route::get('reparer', function () {
    $produits = json_decode(file_get_contents("./produits.json"));
    $users = json_decode(file_get_contents("./users.json"));
    $listeProduitUser = [];
    foreach ($produits as $value) {
        if($value->aReparer == true){
            foreach ($users as $value2) {
                if($value->idUser == $value2->id){
                    $listeProduitUser[] = [$value,$value2];
                }
            }
        }
    }
    return view('reparer',[
        "listeProduitUser" => $listeProduitUser
    ]);
})->name('reparer');

Route::get('acheter', function () {
    $produits = json_decode(file_get_contents("./produits.json"));
    $users = json_decode(file_get_contents("./users.json"));
    $listeProduitUser = [];
    foreach ($produits as $value) {
        if($value->aReparer == false){
            foreach ($users as $value2) {
                if($value->idUser == $value2->id){
                    $listeProduitUser[] = [$value,$value2];
                }
            }
        }
    }
    return view('acheter',[
        "listeProduitUser" => $listeProduitUser
    ]);
})->name('acheter');

Route::get('contacts', function () {
    return view('contacts', [
        'sent' => false
    ]);
})->name('contacts');

Route::post('contacts', function () {
    
    
    return view('contacts', [
        'sent' => true
    ]);
})->name('contacts');

Route::get('afficherProduit/{id}', function ($id) {
    $produits = json_decode(file_get_contents("./produits.json"));
    $users = json_decode(file_get_contents("./users.json"));
    
    $produit = "";
    $user = "";
    $resteImages = [];
    foreach ($produits as $value) {
        if($value->id == $id){
            $produit = $value;
        }
    }
    foreach ($users as $value) {
        if($value->id == $produit->idUser){
            $user = $value;
        }
    }

    foreach ($produit->images as $key => $value) {
        if($key != 0){
            $resteImages[] = $value;
        }
    }

    if( !isset($_SESSION['user']) || ( $_SESSION['user'] === "-1" || $_SESSION['user'] === false )){
        $connected = false;
    }
    else{
        $connected = true;
    }

    return view('afficherProduit', [
        "motifs" => json_decode(file_get_contents("./motifs.json")),
        "problems" => json_decode(file_get_contents("./problems.json")),
        "produit" => $produit,
        "user" => $user,
        "firstImage" => $produit->images[0],
        "resteImage" => $resteImages,
        "connected" => $connected
    ]);
})->name('afficherProduit');

Route::post('mesProduits', function () {
    if( !isset($_SESSION['user']) || ( $_SESSION['user'] === "-1" || $_SESSION['user'] === false )){
        $_SESSION['redirection'] = "mesProduits";
        return redirect('login');
    }
    else{
        $produits = json_decode(file_get_contents("./produits.json"));
        $listeProduitsUser = [];
        foreach ($produits as $value) {
        
            if($value->idUser == $_SESSION['user']){
                if($value->aReparer == false){
                    $value->statut = "A Vendre";
                }
                else{
                    $value->statut = "A Réparer";
                }
                $listeProduitsUser[] = $value;
            }
        }
    
        return view('mesProduits',[
            "produits" => $listeProduitsUser
        ]);
    }
})->name('mesProduits');

Route::get('mesProduits', function () {
    if( !isset($_SESSION['user']) || ( $_SESSION['user'] === "-1" || $_SESSION['user'] === false )){
        $_SESSION['redirection'] = "mesProduits";
        return redirect('login');
    }
    else{
        $produits = json_decode(file_get_contents("./produits.json"));
        $listeProduitsUser = [];
        foreach ($produits as $value) {
        
            if($value->idUser == $_SESSION['user']){
                if($value->aReparer == false){
                    $value->statut = "A Vendre";
                }
                else{
                    $value->statut = "A Réparer";
                }
                $listeProduitsUser[] = $value;
            }
        }
    
        return view('mesProduits',[
            "produits" => $listeProduitsUser
        ]);
    }
})->name('mesProduits');

Route::get('faireProposition', function () {
    return view('faireProposition');
})->name('faireProposition');

Route::get('aPropos', function () {
    return view('aPropos');
})->name('aPropos');

Route::fallback(function() {
    return view('404');
})->name('404');

Route::get('user', function () {
    $found = true;
    $auth = false;
    $leUser = null;
    $id = -1;
    $users = json_decode(file_get_contents("./users.json"));

    if(isset($_SESSION['user']) && $_SESSION['user'] !== "-1" && $_SESSION['user'] !== false){
        //utilisateur connecté
        $auth = true;
        $id = $_SESSION['user'];
        
        foreach($users as $user){
            if($user->id == $id){
                $leUser = $user;
            }
        }
    }
    else{
        $_SESSION['redirection'] = "user";
        return redirect('login');
    }

    return view('monProfil', [
        'auth' => $auth,
        'userObj' => $leUser,
        'avis' => $users,
        'found' => $found
    ]);
})->name("monProfil");

Route::post('user', function (Request $request) {
    $found = true;
    $auth = false;
    $leUser = null;
    $id = -1;
    $fail = true;
    $users = json_decode(file_get_contents("./users.json"));

    if(isset($_SESSION['user']) && $_SESSION['user'] !== "-1" && $_SESSION['user'] !== false){
        //utilisateur connecté
        $auth = true;
        $id = $_SESSION['user'];
        
        foreach($users as $user){
            if($user->id == $id){
                $leUser = $user;
            }
        }
    }
    else{
        $_SESSION['redirection'] = "user";
        return redirect('login');
    }

    //requête de sauvegarde
    if($request->filled(['prenom', 'nom', 'mail', 'adresse', 'postal', 'ville'])){
        $fail = false;
    }

    return view('monProfil', [
        'auth' => $auth,
        'userObj' => $leUser,
        'avis' => $users,
        'found' => $found,
        'fail' => $fail
    ]);
})->name("monProfil");

Route::get('user/{id}', function ($id) {
    $found = false;
    $auth = false;
    $leUser = null;
    $users = json_decode(file_get_contents("./users.json"));

    if(isset($_SESSION['user']) && $_SESSION['user'] == $id){
        //utilisateur connecté
        $auth = true;
    }

    foreach($users as $user){
        if($user->id == $id){
            $found = true;
            $leUser = $user;
        }
    }

    return view('monProfil', [
        'auth' => $auth,
        'userObj' => $leUser,
        'avis' => $users,
        'found' => $found
    ]);
})->name("userProfil");

Route::post('user/{id}', function (Request $request, $id) {
    $found = false;
    $fail = true;
    $auth = false;
    $leUser = null;
    $users = json_decode(file_get_contents("./users.json"));

    if(isset($_SESSION['user']) && $_SESSION['user'] == $id){
        //utilisateur connecté
        $auth = true;

        //requête de sauvegarde
        if($request->filled(['prenom', 'nom', 'mail', 'adresse', 'postal', 'ville'])){
            $fail = false;
        }
    }

    foreach($users as $user){
        if($user->id == $id){
            $found = true;
            $leUser = $user;
        }
    }

    return view('monProfil', [
        'auth' => $auth,
        'userObj' => $leUser,
        'avis' => $users,
        'found' => $found,
        'fail' => $fail
    ]);
})->name("userProfil");

Route::get('signaler', function() {
    return view('signaler',[
        "motifs" => json_decode(file_get_contents("./motifs.json")),
        'annonceSignalee' => "asus"
    ]);
})->name('signaler');

Route::get('testLayout', function() {
    return view('testLayout');
})->name('testLayout');

Route::get('mentionsLegales', function() {
    return view('mentionsLegales', []);
})->name('mentionsLegales');

Route::get('aPropos', function() {
    return view('aPropos', []);
})->name('aPropos');

Route::get('register', function() {
    if(isset($_SESSION['user']) && $_SESSION['user'] !== "-1" && $_SESSION['user'] !== false){
        return redirect('user');
    }
    else{
        return view('register', [
            'fail' => false
        ]);
    }
})->name('register');

Route::post('register', function() {
    $fail = false;

    if(isset($_POST['mail']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['adresse']) && isset($_POST['postal']) && isset($_POST['ville']) && isset($_POST['pass']) && isset($_POST['passcon'])){
        if($_POST['pass'] === $_POST['passcon']){

            //enregistrer en base
            $adresse = strtolower($_POST['adresse']) . ', ' . $_POST['postal'] . ' ' . strtoupper($_POST['ville']);
            $id = 0;
            $users = json_decode(file_get_contents("./users.json"));
            foreach($users as $user){
                $id++;
            }

            $json = array('id' => $id, 'password' => $_POST['pass'], 'nom' => $_POST['nom'], 'prenom' => $_POST['prenom'], 'mail' => $_POST['mail'], 'adresse' => $adresse, 'avis' => array('recu' => array(), 'rediger' => array()), 'admin' => false);

            array_push($users, $json);

            $tosave = json_encode($users);

            file_put_contents("users.json", $tosave, LOCK_EX);

            //terminé
            return view('register', [
                'fail' => 'success'
            ]);
        }
        else{
            return view('register', [
                'fail' => 'pass'
            ]);
        }
    }
    else{
        return view('register', [
            'fail' => 'empty'
        ]);
    }
});

Route::get('login', function() {
    if(isset($_SESSION['user']) && $_SESSION['user'] !== "-1" && $_SESSION['user'] !== false){
        return redirect('user');
    }
    else{
        return view('login', [
            'fail' => false
        ]);
    }
})->name('login');

Route::post('login', function() {
    $fail = true;
    $users = json_decode(file_get_contents("./users.json"));

    foreach($users as $user){
        if($user->mail === $_POST['mail'] && $user->password === $_POST['pass']){
            $_SESSION['user'] = $user->id;
            $fail = false;
        }
    }

    if($fail){
        return view('login', [
            'fail' => $fail
        ]);
    }

    $redirect = "/";
    if(isset($_SESSION['redirection'])){
        $redirect = $_SESSION['redirection'];
    }

    if(!isset($redirect) || $redirect === false){
        return redirect('/');
    }
    else{
        return redirect($redirect);
    }

    
});

Route::get('deconnexion', function() {
    $_SESSION['user'] = false;
    $_SESSION['redirection'] = false;
    return redirect('login');
})->name('deconnexion');
