@extends('layout')

@section('title', 'Acheter')

@section('description', 'Liste des produits mis en ligne attendant un acheteur.')

@section('headers')
<link rel="stylesheet" href="{{ asset('css/reparer_acheter.css') }}">
@endsection

@section('content')
<h1>Acheter un appareil</h1>
<!-- Liste d'annonces pour réparation produit -->
<div class="d-flex flex-row justify-content-around align-items-center flex-wrap">
    <!-- Pour chaque annonce faire -->
    @foreach($listeProduitUser as $item)
    <form action="{{ route('afficherProduit', $item[0]->id) }}" class="d-flex flex-column rpt-block" >
        <div>
            <div class="d-flex flex-row justify-content-center">
                <label><b>{{$item[0]->nom}}</b></label>
            </div>
            <div class="d-flex flex-row justify-content-center">
                <label class="retirer"></label>
                <img src="{{ asset($item[0]->images[0]) }}" alt="image de test" class="thumbnail rpt-block" />
            </div>
            <div class="d-flex flex-row justify-content-center">
                <label><b>Publié par</b> : <a href="{{ route('userProfil', $item[1]->id) }}" > {{ $item[1]->prenom}} {{strtoupper( substr($item[1]->nom, 0, 1)) }}.</a></label><!-- insérer nom user ici -->
            </div>
            <div class="d-flex flex-row justify-content-center">
            <label>Prix d'achat : {{ $item[0]->budgetPrixDeVente}}€</label>
            </div>
            <div class="d-flex flex-row justify-content-center">
                <label>Date de publication : {{ $item[0]->datePublication}}</label>
            </div>
            <div class="d-flex flex-row justify-content-center">
                <input class="btn rpt-button col-md-6 col-sm-8 col-10" type="submit" value="Consulter"/>
            </div>
        </div>
    </form>
    @endforeach
</div>
<div class="row"><p class="invisible">ceci est invisible</div>

@endsection