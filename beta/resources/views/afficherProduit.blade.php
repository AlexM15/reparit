@extends('layout')

@section('title', 'Asus FX-6985 écran cassé')

@section('description', 'Description complète du produit avec possibilité de faire une proposition ou un signalement sur le produit.')

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/afficherProduit.css') }}">
    <script type="text/javascript" src="{{ asset('js/afficherProduit.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/signaler.js')}}"></script>
@endsection


@section('content')
<div id="contenu" class="d-flex flex-wrap flex-row align-items-center justify-content-around">
    <div id="contenu-image" class="d-flex flex-column">
        <h1>{{ $produit->nom }}</h1>
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target=".carousel" data-slide-to="0" class="active"></li>
                @for($i=1; $i < count($resteImage) +1; $i++)
                <li data-target=".carousel" data-slide-to="{{ $i }}"></li>
                @endfor
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100 img-fluid" src="{{ asset($firstImage)}}" alt="Un ordi cassé">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100 img-fluid" src="{{ asset('images/2.png')}}" alt="Un ordi cassé">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#carousel" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a> 
        </div>
    </div>
    <div id="details" class="d-flex flex-column justify-content-center">
        <div>
            <p><b>Prix :</b> {{ $produit->budgetPrixDeVente }}</p>
            <p><b>Problème(s) : </b>
                <ul>
                    @foreach($produit->problems as $Idprob)
                        @foreach($problems as $problem)
                            @if($problem->id == $Idprob)
                            <li class="problem">{{$problems[$Idprob]->text}}</li>
                            @endif
                        
                        @endforeach
                    @endforeach
                </ul>
            </p>
            <p><b>Description du problème :</b> {{ $produit->description }}</p>
            <p><b>Date de publication :</b> {{ $produit->datePublication }} </p>
            <p><b>Demandeur : </b><a href="{{ route('userProfil', $user->id) }}" >{{ $user->login}}</a><br/>
            <b>Avis moyen utilisateur :</b> 4.2/5</p>
        </div>
        <div>
            @if(!$connected)
                <a href="{{route("login") }}"><button id="btn-proposition" class="rpt-button">Faire une proposition</button></a>
            @else
                <button id="btn-proposition" class="rpt-button">Faire une proposition</button>
            @endif
            <button id="signaler-desktop" class="rpt-button-red rpt-button">Signaler cette annonce</button>
            <button id="signaler-mobile" class="rpt-button-red rpt-button"></button>
        </div>
    </div>

    <div id="proposition">
        <form class="rpt-block" method="post" action="" class="">
            <label for="prix-min">Meilleure offre de réparation :</label>
            <input id="prix-min" type="text" class="form-control align-text" value="30 €" disabled>
            <br/>
            <label for="nouveau-prix">Montant de votre offre :</label>
            <input id="nouveau-prix" type="number" class="form-control align-text" placeholder="Votre prix" required>
            <br/>
            <label for="description">Description de votre offre :</label>
            <br/>
            <textarea id="description" rows="4" name="description" class="rpt-block" required></textarea>
            <br/>
            <input type="submit" class="rpt-button" name="valider-proposition"></button>
        </form>
    </div>
</div>
<div id="popup-s">
    <div id="popup-inner" class="container rpt-block">
        <img id="close" src="{{ asset('images/close.png') }}">
        <div class="row justify-content-center">
            <label>Vous voulez signaler l'annonce de {{$produit->nom}}.</label>
        </div>
        <div class="row justify-content-center">
            <label><b>Pour quel motif souhaitez-vous signaler cette annonce ?</b></label>
        </div>
        <div id="problemes" class="row justify-content-center">
            <select class="js-basic-multiple form-control col-md-8 col-sm-7 col-10" name="motif[]" multiple="multiple">
                @foreach($motifs as $motif)
                    <option value="{{ $motif->id }}">{{ $motif->text }}</option>
                @endforeach
            </select>
        </div>
        
        <div class="row justify-content-center">
            <label><u>Argumentation</u></label>
        </div>
        <div class="row justify-content-center">
            <textarea rows="5" name="argumentation" class="col-md-8 col-sm-10 col-11 rpt-block"></textarea>
        </div>
        <div class="row justify-content-center">
            <input class="btn rpt-button" type="submit" value="Valider" onclick="signalement();"/>
        </div>
    </div>        
</div>
@endsection