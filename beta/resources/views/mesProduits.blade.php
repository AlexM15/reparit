@extends('layout')

@section('title', 'Mes Produits')

@section('description', 'Liste des produits mis en ligne par un utilisateur.')

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/mesProduits.css') }}">
@endsection

@section('content')
<h1>Mes produits</h1>
<!-- Liste d'annonces pour réparation produit -->
<div id="contenu">
    <!-- Pour chaque annonce faire -->
    @foreach($produits as $produit)
        <hr>
        <div class="row rpt-block">
            <div id="item-left" class="d-flex flex-column">
                <div id="image-produit" class="d-flex flex-row">
                    <a href={{ route('afficherProduit',$produit->id) }}><img src=" {{ asset($produit->images[0])}}" alt="image de test" class="thumbnail img-responsive" /></a>
                </div>
            </div>
            <div id="item-right" class="d-flex flex-column justify-content-center">
                <div id="nom-produit" class="d-flex flex-row">
                <h2><a href={{ route('afficherProduit',$produit->id) }}><strong> {{ $produit->nom }}</strong></a></h2>
                </div>
                <div class="d-flex flex-row">
                <p><strong>Budget :</strong> {{ $produit->budgetPrixDeVente }} €</p>
                </div>
                <div class="d-flex flex-row">
                    <p><strong>Date de publication :</strong> {{ $produit->datePublication}} </p>
                </div>
                <div class="d-flex flex-row">
                    <p><strong>État :</strong> {{ $produit->statut }} </p>
                </div>
                <button id="supprannonce" class="rpt-button rpt-button-red">Supprimer l'annonce</button>
            </div>
        </div>
    @endforeach
</div>
@endsection