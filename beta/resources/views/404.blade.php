@extends("layout")

@section("title", 'Page 404')

@section('description', 'Vous avez atteint la mauvaise page !')

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/404.css') }}">
@endsection

@section("content")
  <div id="contenu-404">
    <img id="t-pose" class="center" src="{{ asset('images/t-pose.JPG') }}" alt="L'équipe de Répar'IT T-pose.">
    <h1 class="center">404, cette page n'existe pas !</h1>
    <p id="description" class="center">L'équipe n'a pas réussi à charger ses animations à temps :(</p>
  </div>
@endsection
