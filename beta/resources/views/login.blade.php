@extends('layout')

@section('title', 'Se connecter')

@section('description',"Ici vous pourrez vous connecter à notre plateforme Répar'IT.")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')  
<div id="login-page">
    <div id="login" class="rpt-block">
        <h1>Se connecter</h1>
        <hr>
        <div class="login-content align-text">
            @if (isset($_SESSION['user']) && $_SESSION['user'] !== false && $_SESSION['user'] !== "-1")
                <p>
                    <h2>Vous êtes déjà connecté !</h2><br/>
                    <a href="{{ route('deconnexion') }}" class="rpt-button">Déconnexion</a>
                </p>
            @else
                <form method="post">
                    @csrf
                    @if ($fail === true)
                        <p>
                            <strong style="color: red;">Mauvais identifiant ou mot de passe !</strong>
                            {{ $_SESSION['user'] = false }}
                        </p>
                    @endif
                    <p>
                        <strong>Adresse mail :</strong>
                        <div class="col-5 align-item">
                            <input name="mail" type="email" class="form-control align-text" placeholder="mail@exemple.com">
                        </div>
                    </p>
                    <p>
                        <strong>Mot de passe :</strong>
                        <div class="col-5 align-item">
                            <input name="pass" type="password" class="form-control align-text" placeholder="************">
                        </div>
                    </p>
                    <input type="submit" class="rpt-button" value="Connexion">
                </form>
                <br/>
                Pas de compte ? <a href="{{ route('register') }}">S'enregistrer</a>
            @endif
        </div>
    </div>
</div>
@endsection
