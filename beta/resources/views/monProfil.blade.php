@extends('layout')

@section('title', 'Mon profil')

@section('description', "Profil d'un utilisateur de Répar'IT")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/profil.css') }}">
@endsection

@section('content')
@if ($found === false)
    <!-- TO-DO: Page user non trouvée -->
    <div class="rpt-block align-text">
        <h1>Erreur !</h1>
        <h2>Cet utilisateur n'existe pas !</h2>
    </div>
@else
    @if ($auth === true)
        <div id="profil-page">
            <div id="profil" class="rpt-block">
                <h1>Mon profil</h1>
                @if (isset($fail) && $fail === false)
                    <p class="align-text">
                        <strong style="color: green;">Modifications enregistrés !</strong>
                    </p>
                @else
                    @if (isset($fail) && $fail === true)
                        <p class="align-text">
                            <strong style="color: red;">Veuillez renseigner la partie "Informations Utilisateur" !</strong>
                        </p>
                    @endif
                @endif
                <form action="{{ route('deconnexion') }}" method="get">
                    <input type="submit" class="align-item-force rpt-button rpt-button-red" value="Se déconnecter"/>
                </form>
                <hr>
                <div class="profil-content align-text">
                    <h2>Informations Utilisateur</h2>
                    <br/>
                    <form action="#" method="post">
                        @csrf
                        <p>
                            <strong>Prénom / Nom :</strong>
                            <div class="col-5 align-item">
                                <input name="prenom" type="text" class="form-control align-text" placeholder="Prénom" value="{{ $userObj->prenom }}" required>
                                <input name="nom" type="text" class="form-control align-text" placeholder="Nom" value="{{ $userObj->nom }}" required>
                                <br/>
                            </div>
                        </p>
                        <p>
                            <strong>Adresse mail :</strong>
                            <div class="col-5 align-item">
                                <input name="mail" type="email" class="form-control align-text" placeholder="mail@exemple.com" value="{{ $userObj->mail }}" required>
                            </div>
                        </p>
                        <p>
                            <?php
                                $fulladdr = $userObj->adresse;
                                $addr = substr($fulladdr, 0, strrpos($fulladdr, ','));
                                $postal = substr($fulladdr, strrpos($fulladdr, ',') + 2);
                                $city = substr($postal, strpos($postal, ' ') + 1);
                                $postal = substr($postal, 0, strrpos($postal, ' '));
                            ?>
                            <strong>Adresse de livraison :</strong>
                            <div class="col-5 align-item">
                                <input name="adresse" type="text" class="form-control align-text" placeholder="Adresse" value="{{ $addr }}" required>
                                <input name="postal" type="text" class="form-control align-text" placeholder="Code postal" value="{{ $postal }}" required>
                                <input name="ville" type="text" class="form-control align-text" placeholder="Ville" value="{{ $city }}" required>
                            </div>
                        </p>
                        <hr>
                        <h3>Modifier votre mot de passe</h3>
                        <br/>
                        <p>
                            <strong>Mot de passe (actuel) :</strong>
                            <div class="col-5 align-item">
                                <input id="pass" type="password" class="form-control align-text" placeholder="************">
                            </div>
                        </p>
                        <p>
                            <strong>Nouveau mot de passe :</strong>
                            <div class="col-5 align-item">
                                <input id="newpass" type="password" class="form-control align-text" placeholder="************">
                            </div>
                        </p>
                        <p>
                            <strong>Ressaisir le mot de passe :</strong>
                            <div class="col-5 align-item">
                                <input id="newpass" type="password" class="form-control align-text" placeholder="************">
                            </div>
                        </p>
                        <br/>
                        <input type="submit" class="rpt-button" value="Sauvegarder les modifications"/>
                    </form>
                    <form action="{{ route('mesProduits') }}" method="post">
                        @csrf
                        <br/>
                        <input type="submit" class="rpt-button rpt-button-blue" value="Consulter mes produits"/>
                    </form>
                </div>
            </div>
            
            <div id="avis" class="rpt-block">
                <h1>Mes avis</h1>
                <hr>
                <div class="avis-content align-text">
                    <h2>Avis reçus</h2>
                    <br/>
                    @foreach($avis as $user)
                        @if ($user->id == $userObj->id)
                            <?php $unUser = $user ?>
                        @endif
                    @endforeach
                    @foreach($unUser->avis->recu as $avi)
                        @foreach($avis as $user)
                            @if ($user->id == $avi->id)
                                <a href="{{ route('userProfil', $user->id) }}"><u>Avis de <strong>{{ $user->prenom }} : </strong></u></a><br/>
                                {{ $avi->commentaire }}
                            @endif
                        @endforeach
                    @endforeach
                    <hr>
                    <h2>Avis rédigés</h2>
                    <br/>
                    @foreach($unUser->avis->rediger as $avi)
                        @foreach($avis as $user)
                            @if ($user->id == $avi->id)
                                <a href="{{ route('userProfil', $user->id) }}"><u>Avis rédigé pour <strong>{{ $user->prenom }} : </strong></u></a><br/>
                                {{ $avi->commentaire }}
                            @endif
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    @else
        <!-- récupération des infos -->
        <?php
            foreach($avis as $user){
                if($user->id == $userObj->id){
                    $prenom = $user->prenom;
                    $nom = $user->nom;
                    $mail = $user->mail;
                    $unUser = $user;
                }
            }
        ?>

        <div id="profil-page">
            <div id="profil" class="rpt-block">
                <h1>Profil de {{ $prenom }}</h1>
                <hr>
                <div class="profil-content align-text">
                    <h2>Informations Utilisateur</h2>
                    <br/>
                    <p>
                        <strong>Utilisateur :</strong><br/>
                        {{ $prenom }} {{ $nom }}
                    </p>
                    <p>
                        <strong>Adresse mail :</strong><br/>
                        {{ $mail }}
                    </p>
                </div>
            </div>
            <div id="avis" class="rpt-block">
                <h1>Mes avis</h1>
                <hr>
                <div class="avis-content align-text">
                    <h2>Avis reçus</h2>
                    <br/>
                    @foreach($unUser->avis->recu as $avi)
                        @foreach($avis as $leUser)
                            @if ($leUser->id == $avi->id)
                                <a href="{{ route('userProfil', $leUser->id) }}"><u>Avis de <strong>{{ $leUser->prenom }} : </strong></u></a><br/>
                                {{ $avi->commentaire }}
                            @endif
                        @endforeach
                    @endforeach
                    <hr>
                    <h2>Avis rédigés</h2>
                    <br/>
                    @foreach($unUser->avis->rediger as $avi)
                        @foreach($avis as $leUser)
                            @if ($leUser->id == $avi->id)
                                <a href="{{ route('userProfil', $leUser->id) }}"><u>Avis rédigé pour <strong>{{ $leUser->prenom }} : </strong></u></a><br/>
                                {{ $avi->commentaire }}
                            @endif
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
    @endif
@endif
@endsection
