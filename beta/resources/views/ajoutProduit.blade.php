@extends('layout')

@section('title', 'Ajouter un produit')

@section('description', 'Ici vous pouvez ajouter un produit à vendre ou à réparer.')

@section('headers')
<link rel="stylesheet" href="{{ asset("css/ajoutProduit.css") }}">
<script src="{{ asset("js/ajoutProduit.js") }}"></script>
@endsection

@section('content')
<h1>Ajouter un appareil</h1>
<form class="row justify-content-center"  method="post">
    @csrf
    <div id="block-image" class="col-md-5 rpt-block">
        <div id="image-list" class="row justify-content-center"></div>
        <hr>
        <p>Cliquez sur une image pour la supprimer.</p>
        <input id="add-image" type="file" accept="image/png, image/jpeg" hidden>
        <!--<input type="hidden" name="images[]">-->
        <div>
            <label for="add-image" class="rpt-button">Ajouter une image</label>
            <label class="rpt-button rpt-button-red">Supprimer toutes les images</label>           
        </div>        
    </div>
    <div class="col-md-6 col-sm-11 rpt-block">
        <div class="row justify-content-center">
            <label for="input-name" class="col-md-5 col-sm-10 justify-content-center"><u>Modèle de l'appareil :</u></label>
            <input id="input-name" class="col-md-10 col-sm-10 rpt-block" type="text" placeholder="Samsung Galaxy S9" required>
        </div>
        <div class="row justify-content-center invisible">espace</div>
        <div class="row justify-content-center">
            <select class="col-md-10 col-sm-11" multiple="multiple" required>
                @foreach($problems as $problem)
                    <option value="{{ $problem->id }}">{{ $problem->text }}</option>
                @endforeach
            </select>
        </div>
        <div class="row justify-content-center align-items-center">
            <label for="problem-desc" class="col-md-5 col-sm-10 col-12"><u>Description du problème :</u></label>
            <textarea id="problem-desc" class="col-md-10 col-sm-11 col-12 rpt-block" rows="4"></textarea>
        </div>
        
    </div>
    <div class="col-md-6 col-sm-11 rpt-block">
        <div class="d-flex flex-row flex-wrap align-items-center justify-content-between">
            <label for="problem-desc" class="col-md-5 col-sm-10 col-12"><u>Prix minimum estimé :</u></label>
            <input type="text" class="col-md-5 col-sm-10 col-12 rpt-block" disabled value="70 €">
        </div>
        <div class="d-flex flex-row flex-wrap align-items-center justify-content-between">
            <label class="flex-column">À réparer :</label>
            <input id="checkbox-reparer" class="flex-column" type="checkbox"><input class="flex-column rpt-block" type="number" id="budget" disabled>
        </div>
        <div class="d-flex flex-row flex-wrap align-items-center justify-content-between">
            <label class="flex-column">À vendre : </label>
            <input id="checkbox-vendre" class="flex-column" type="checkbox"><input class="flex-column rpt-block" type="number" id="prixVente" disabled>
        </div>
        <input class="rpt-button col-md-4" type="submit">
    </div> 
</form>
@endsection
