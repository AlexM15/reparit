@extends('layout')
@section("title", "Accueil")
@section('description', "La plateforme de réparation d'appareils électroniques qui vous facilite la vie.")

@section('headers')
<link rel="stylesheet" href="{{ asset('css/accueil.css') }}">
<link rel="stylesheet" href="{{ asset('css/reparer_acheter.css') }}">
<script rel="stylesheet" src="{{ asset("js/index.js") }}"></script>
<script rel="stylesheet" src="{{ asset("js/accueil.js") }}"></script>
@endsection

@section('content')  

<h1 class='col-12'>Bienvenue sur Répar'IT !</h1>
<div class="container justify-content-center">
    <div class="row justify-content-center">
        <div class="col-md-3 col-sm-8 col-10 text-center rpt-block info rpt-pulse">
            <div class="row lien"><a href="{{ route('reparer') }}"><img src="{{ asset('images/icone_reparer0.png') }}" alt="Icone réparation d'ordinateur" class="reduire"/></a></div>
            <div class="row"><p>Vous êtes passioné de réparation ou de bricolage d'appareils numériques ? <br/>Notre grand panel d'annonces saura vous satisfaire.</p></div>
        </div>
        <div class="col-md-3 col-sm-8 col-10 text-center rpt-block info rpt-pulse">
            <div class="row lien"><a href="{{ route('ajoutProduit') }}"><img src="{{ asset('images/user0.png') }}" alt="Icone réparation d'ordinateur" class="reduire"/></a></div>
            <div class="row"><p>Vous voulez faire réparer l'un de vos appareils cassé ? <br/>Rien de plus simple : inscrivez-vous sur Répar'IT et créer une annonce concernant votre appareil cassé en indiquant ses défauts.</p></div>
        </div>
        <div class="col-md-3 col-sm-8 col-10 text-center rpt-block info rpt-pulse">
            <div class="row lien"><a href="{{ route('acheter') }}"><img src="{{ asset('images/icone_acheter0.png') }}" alt="Icone achat d'ordinateur" class="reduire"/></a></div>
            <div class="row"><p>Vous souhaitez acheter un appareil d'occasion à réparer vous-même pour le revendre ou pour votre propre consommation ?<br/>Alors cette page est faite pour vous.</p></div>
        </div>
    </div>
</div>
<hr>
<h2>Dernières annonces mises en ligne</h2>
<div class="row justify-content-center">
    <form class="form-inline" method="post" action="" >
        <label for='search'>Rechercher: &nbsp;</label>
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <!-- Default inline 1-->
        <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" class="custom-control-input" id="reparer" checked>
            <label class="custom-control-label" for="reparer">Je répare</label>
        </div>
        <!-- Default inline 2-->
        <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" class="custom-control-input" id="vendre" checked>
            <label class="custom-control-label" for="vendre">J'achète</label>
        </div>
    </form>
</div>
<div class="row invisible">ceci est invisible</div> 

<div class="d-flex flex-row justify-content-around align-items-start flex-wrap">
    <div id="blocReparer">
        <h3>Appareils à réparer</h3>
        <div class="d-flex flex-column" >
        @foreach($listeProduitUserReparer as $item)
            <div class="d-flex flex-row justify-content-around" >
                @include("formulaireAccueil")
            </div>
        @endforeach
        </div>
    </div>
    <div id="blocVendre">
        <h3>Appareils à vendre</h3>
        <div class="d-flex flex-column">
            @foreach($listeProduitUserVendre as $item)
                <div class="d-flex flex-row justify-content-around" >
                    @include("formulaireAccueil")
                </div>
            @endforeach
        </div>
    </div>
</div>


@endsection
