@extends('layout')

@section('title', "S'enregistrer")

@section('description',"S'inscrire à Répar'IT.")

@section('headers')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

@section('content')  
<div id="login-page">
    <div id="login" class="rpt-block">
        <h1>Créez votre compte</h1>
        <hr>
        <div class="login-content align-text">
            @if ($fail === "success")
                <p>
                    <h2>Vous êtes maintenant enregistré !</h2>
                    <br/>
                    <a class="rpt-button" href="{{ route('login') }}">Se connecter</a>
                </p>
            @else
                <form method="post">
                    @csrf
                    @if ($fail === "empty")
                        <p>
                            <strong style="color: red;">Veuillez remplir tous les champs !</strong>
                        </p>
                    @endif
                    @if ($fail === "pass")
                        <p>
                            <strong style="color: red;">Les champs mot de passe ne correspondent pas !</strong>
                        </p>
                    @endif
                    <p>
                        <strong>Adresse mail :</strong>
                        <div class="col-5 align-item">
                            <input name="mail" type="email" class="form-control align-text" placeholder="mail@exemple.com" required>
                        </div>
                    </p>
                    <p>
                        <strong>Nom :</strong>
                        <div class="col-5 align-item">
                            <input name="nom" type="text" class="form-control align-text" placeholder="Hatsune" required>
                        </div>
                    </p>
                    <p>
                        <strong>Prénom :</strong>
                        <div class="col-5 align-item">
                            <input name="prenom" type="text" class="form-control align-text" placeholder="Miku" required>
                        </div>
                    </p>
                    <p>
                        <strong>Adresse de livraison :</strong>
                        <div class="col-5 align-item">
                            <input name="adresse" type="text" class="form-control align-text" placeholder="15 Rue de Naudet" required>
                            <input name="postal" type="text" class="form-control align-text" placeholder="33175" required>
                            <input name="ville" type="text" class="form-control align-text" placeholder="Gradignan" required>
                        </div>
                    </p>
                    <p>
                        <strong>Mot de passe :</strong>
                        <div class="col-5 align-item">
                            <input name="pass" type="password" class="form-control align-text" placeholder="************" required>
                        </div>
                    </p>
                    <p>
                        <strong>Mot de passe (confirmation) :</strong>
                        <div class="col-5 align-item">
                            <input name="passcon" type="password" class="form-control align-text" placeholder="************" required>
                        </div>
                    </p>
                    <input type="submit" class="rpt-button" value="Valider">
                </form>
            @endif
        </div>
    </div>
</div>
@endsection
