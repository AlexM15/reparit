

<form action="{{ route('afficherProduit', $item[0]->id) }}" class="d-flex flex-column rpt-block" >
        <div class="d-flex flex-row justify-content-center">
            <label><b>{{$item[0]->nom}}</b></label>
        </div>
        <div class="d-flex flex-row justify-content-center">
            <label class="retirer"></label>
            <img src="{{ asset($item[0]->images[0]) }}" alt="image de test" class="thumbnail rpt-block" />
        </div>
        <div class="d-flex flex-row justify-content-center">
            <label><b>Publié par</b> : <a href="{{ route('userProfil', $item[1]->id) }}" >{{ $item[1]->prenom}} {{strtoupper( substr($item[1]->nom, 0, 1)) }}.</a></label><!-- insérer nom user ici -->
        </div>
        <div id="labelVendre">
            <div class="d-flex flex-row justify-content-center">
                <label>Prix d'achat : {{$item[0]->budgetPrixDeVente }} €</label>
            </div>
        </div>
        <div id="labelReparer">
            <div class="d-flex flex-row justify-content-center">
                <label>Budget : {{$item[0]->budgetPrixDeVente }} €</label>
            </div>
        </div>
        <div class="d-flex flex-row justify-content-center">
            <label>Date de publication : {{$item[0]->datePublication}}</label>
        </div>
        <div class="d-flex flex-row justify-content-center">
            <input class="btn rpt-button col-md-6 col-sm-8 col-10" type="submit" value="Consulter"/>
        </div>
    </form>