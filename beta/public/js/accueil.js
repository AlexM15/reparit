$(document).ready(function(){
    $('#reparer').change(function(){
        if ( $(this).is(':checked')) {
            $('#blocReparer').show();
            $('#blocVendre').children("div").addClass("flex-column").removeClass("flex-row justify-content-around flex-wrap");
        }
        else {
            if($('#vendre').is(':checked')){
                $('#blocReparer').hide();
                $('#blocVendre').children("div").removeClass("flex-column").addClass("flex-row justify-content-around flex-wrap");
            }
            else{
                $('#reparer').prop( "checked", true );
            }
        }                   
    });

    $('#vendre').change(function(){
        if ($(this).is(':checked')) {
            $('#blocVendre').show();
            $('#blocReparer').children("div").addClass("flex-column").removeClass("flex-row justify-content-around flex-wrap");
        }
        else {
            if($('#reparer').is(':checked')){
                $('#blocVendre').hide();
                $('#blocReparer').children("div").removeClass("flex-column").addClass("flex-row justify-content-around flex-wrap");
            }
            else{
                $('#vendre').prop( "checked", true );
            }
        }                   
    });
    $('#blocReparer #labelVendre').hide();
    $('#blocVendre #labelReparer').hide();
});