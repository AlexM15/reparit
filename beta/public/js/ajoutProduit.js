let IMAGES = [];
function addImage(file) {
    if (IMAGES.length == 4) return false;
    let reader = new FileReader();
    reader.onload = e => {
        IMAGES.push({file: file, src: e.target.result});
        updateList();
    }
    reader.readAsDataURL(file);   
}
function removeImg(i) {
    IMAGES.splice(i, 1);
    
    updateList();
}
function clearList() {
    IMAGES = [];
    updateList();
}
function updateList() {
    //$("input[type=hidden]").val(IMAGES.map(img => img.src));
    $("#image-list").children().remove();
    let i = 0;
    for (let {file, src} of IMAGES) {
        let id = i++;
        $("#image-list").append('<div class="col-5 rpt-block"></div>');       
        $("#image-list div:last-child").css("background-image", `url(${src})`);
        $("#image-list div:last-child").click(() => removeImg(id));
    }
    $("#image-list div").css("height", IMAGES.length > 2 ? "12vh" :"24vh");
}

$(document).ready(function(){
    // select 2
    $("#content select").select2({
        placeholder: 'Choisir un problème'
    });

    // images
    $("#block-image label:last-child").click(clearList);
    $("input[type=file]").change(function() {
        let file = this.files[0];
        if (file) addImage(file);     
    });

    // checkboxes
    $("#checkbox-reparer").change(function() {
        $("#budget").attr("disabled", !$(this).is(":checked"));
    });
    $("#checkbox-vendre").change(function() {
        $("#prixVente").attr("disabled", !$(this).is(":checked"));
    });
});