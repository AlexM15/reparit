$(document).ready(function(){
    $('#btn-proposition').click(function(){
        $('#proposition').show();
        $('#proposition').addClass("animated slideInDown");         
    });

    $('#signaler-desktop').click(function(){
        $('#popup-s').show();
        $('#popup-s').addClass("animated fadeIn");
        $(".js-basic-multiple").select2({
            placeholder: 'Choisir un motif ...'
        });
        $(window).scrollTop(0);
    });
    
    $('#signaler-mobile').click(function(){
        $('#popup-s').show();
        $('#popup-s').addClass("animated fadeIn");
        $(".js-basic-multiple").select2({
            placeholder: 'Choisir un motif ...'
        });
        $(window).scrollTop(0);
    });

    $('#close').click(function(){
        $('#popup-s').hide();
    });
});